import {
  getVouchersReq,
  getVoucherReq,
  addVoucherReq,
  editVoucherReq,
  deleteVoucherReq
  // searchDataReq
} from '@/api/voucherApi'

const state = {
  vouchers: [],
  voucherFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_VOUCHERS: (state, data) => {
    state.vouchers = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.voucherFilters = data
  }
}

const actions = {
  loadVouchers({ state, commit }) {
    return new Promise((resolve, reject) => {
      getVouchersReq({ ...state.voucherFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_VOUCHERS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadVouchers')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadVouchers')
  },
  changeVoucherFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadVoucher({ commit }, id) {
    return new Promise((resolve, reject) => {
      getVoucherReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addVoucher({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addVoucherReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editVoucher({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editVoucherReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteVoucher({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteVoucherReq(id)
        .then((res) => {
          dispatch('loadVouchers')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  vouchers: (state) => state.vouchers,
  voucherFilters: (state) => state.voucherFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

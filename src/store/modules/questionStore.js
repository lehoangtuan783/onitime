import {
  getQuestionsReq,
  getQuestionReq,
  addQuestionReq,
  editQuestionReq,
  deleteQuestionReq
  // searchDataReq
} from '@/api/questionApi'

const state = {
  questions: [],
  questionFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_QUESTIONS: (state, data) => {
    state.questions = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.questionFilters = data
  }
}

const actions = {
  loadQuestions({ state, commit }) {
    return new Promise((resolve, reject) => {
      getQuestionsReq({ ...state.questionFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_QUESTIONS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadQuestions')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadQuestions')
  },
  changeQuestionFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadQuestion({ commit }, id) {
    return new Promise((resolve, reject) => {
      getQuestionReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addQuestion({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addQuestionReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editQuestion({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editQuestionReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteQuestion({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteQuestionReq(id)
        .then((res) => {
          dispatch('loadQuestions')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  questions: (state) => state.questions,
  questionFilters: (state) => state.questionFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

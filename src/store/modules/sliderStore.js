import { getSliderPagedReq, getSliderReq, addSliderReq, editSliderReq, deleteSliderReq } from '@/api/sliderApi'

const state = {
  sliders: [],
  sliderFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_SLIDERS: (state, data) => {
    state.sliders = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.sliderFilters = data
  }
}

const actions = {
  loadSliders({ state, commit }) {
    return new Promise((resolve, reject) => {
      getSliderPagedReq({ ...state.sliderFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_SLIDERS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadSliders')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadSliders')
  },
  changeSliderFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadSlider({ commit }, id) {
    return new Promise((resolve, reject) => {
      getSliderReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addSlider({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addSliderReq({
        ...payload,
        imageIds: payload.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editSlider({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editSliderReq(payload.id, {
        ...payload.data,
        imageIds: payload.data.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteSlider({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteSliderReq(id)
        .then((res) => {
          dispatch('loadSliders')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  sliders: (state) => state.sliders,
  sliderFilters: (state) => state.sliderFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

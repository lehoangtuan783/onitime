import {
  getUserPagedReq,
  getUserReq,
  addUserReq,
  editUserReq,
  deleteUserReq
  // searchDataReq
} from '@/api/userApi'

const state = {
  users: [],
  userFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_USERS: (state, data) => {
    state.users = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.userFilters = data
  }
}

const actions = {
  loadUsers({ state, commit }) {
    return new Promise((resolve, reject) => {
      getUserPagedReq({ ...state.userFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_USERS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadUsers')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadUsers')
  },
  changeUserFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadUser({ commit }, id) {
    return new Promise((resolve, reject) => {
      getUserReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addUserReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editUserReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteUser({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteUserReq(id)
        .then((res) => {
          dispatch('loadUsers')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  users: (state) => state.users,
  userFilters: (state) => state.userFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import {
  getCarRentalPagedReq,
  getCarRentalReq,
  addCarRentalReq,
  editCarRentalReq,
  deleteCarRentalReq
  // searchDataReq
} from '@/api/carRentalApi'

const state = {
  carRentals: [],
  carRentalFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CAR_RENTALS: (state, data) => {
    state.carRentals = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.carRentalFilters = data
  }
}

const actions = {
  loadCarRentals({ state, commit }) {
    return new Promise((resolve, reject) => {
      getCarRentalPagedReq({ ...state.carRentalFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CAR_RENTALS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadCarRentals')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadCarRentals')
  },
  changeCarRentalFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadCarRental({ commit }, id) {
    return new Promise((resolve, reject) => {
      getCarRentalReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addCarRental({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addCarRentalReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editCarRental({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editCarRentalReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteCarRental({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteCarRentalReq(id)
        .then((res) => {
          dispatch('loadCarRentals')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  searchData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      searchDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  carRentals: (state) => state.carRentals,
  carRentalFilters: (state) => state.carRentalFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

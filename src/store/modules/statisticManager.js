import { getBookingDay } from '@/api/statisticApi'

const state = {
  bookingDayPaged: []
}

const mutations = {
  SET_BOOKING_DAY_PAGED: (state, data) => {
    state.bookingDayPaged = data
  }
}

const actions = {
  loadBookingDay({ commit }, payload) {
    getBookingDay(payload)
      .then((res) => {
        commit('SET_BOOKING_DAY_PAGED', res.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }
}

const getters = {
  bookingDayPaged: (state) => state.bookingDayPaged
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

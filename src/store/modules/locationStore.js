import { getLocationsReq, getLocationReq, addLocationReq, editLocationReq, deleteLocationReq } from '@/api/locationApi'

const state = {
  locations: [],
  locationFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_LOCATIONS: (state, data) => {
    state.locations = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.locationFilters = data
  }
}

const actions = {
  loadLocations({ state, commit }) {
    return new Promise((resolve, reject) => {
      getLocationsReq({ ...state.locationFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_LOCATIONS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadLocations')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadLocations')
  },
  changeLocationFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadLocation({ commit }, id) {
    return new Promise((resolve, reject) => {
      getLocationReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addLocation({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addLocationReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editLocation({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editLocationReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteLocation({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteLocationReq(id)
        .then((res) => {
          dispatch('loadLocations')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  searchData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      searchDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  locations: (state) => state.locations,
  locationFilters: (state) => state.locationFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import {
  getConfigPagedReq,
  getConfigReq,
  addConfigReq,
  editConfigReq,
  deleteConfigReq
  // searchDataReq
} from '@/api/configApi'

const state = {
  configs: [],
  configFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CONFIGS: (state, data) => {
    state.configs = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.configFilters = data
  }
}

const actions = {
  loadConfigs({ state, commit }) {
    return new Promise((resolve, reject) => {
      getConfigPagedReq({ ...state.configFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CONFIGS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadConfigs')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadConfigs')
  },
  changeConfigFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadConfig({ commit }, id) {
    return new Promise((resolve, reject) => {
      getConfigReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addConfig({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addConfigReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editConfig({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editConfigReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteConfig({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteConfigReq(id)
        .then((res) => {
          dispatch('loadConfigs')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  configs: (state) => state.configs,
  configFilters: (state) => state.configFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

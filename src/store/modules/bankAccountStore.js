import {
  getBankAccountPagedReq,
  getBankAccountReq,
  addBankAccountReq,
  editBankAccountReq,
  deleteBankAccountReq
} from '@/api/bankAccountApi'

const state = {
  bankAccounts: [],
  bankAccountFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CAR_TYPES: (state, data) => {
    state.bankAccounts = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.bankAccountFilters = data
  }
}

const actions = {
  loadBankAccounts({ state, commit }) {
    return new Promise((resolve, reject) => {
      getBankAccountPagedReq({ ...state.bankAccountFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CAR_TYPES', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadBankAccounts')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadBankAccounts')
  },
  changeBankAccountFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadBankAccount({ commit }, id) {
    return new Promise((resolve, reject) => {
      getBankAccountReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addBankAccount({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addBankAccountReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editBankAccount({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editBankAccountReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteBankAccount({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteBankAccountReq(id)
        .then((res) => {
          dispatch('loadBankAccounts')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  searchData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      searchDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  bankAccounts: (state) => state.bankAccounts,
  bankAccountFilters: (state) => state.bankAccountFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

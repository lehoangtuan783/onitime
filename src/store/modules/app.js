import defaultSettings from '@/settings'
const state = {
  sidebar: {
    opened: true,
    withoutAnimation: false
  },
  loadingStack: 0,
  device: 'desktop',
  settings: defaultSettings,
  cachedViews: []
}
/*mutations建议以M_开头*/
const mutations = {
  /*
   * data:ObjType
   * such as {sidebarLogo:false}
   * */
  M_settings: (state, data) => {
    state.settings = { ...state.settings, ...data }
  },
  M_sidebar_opened: (state, data) => {
    state.sidebar.opened = data
  },
  M_toggleSideBar: (state) => {
    state.sidebar.opened = !state.sidebar.opened
  },

  /*keepAlive缓存*/
  M_ADD_CACHED_VIEW: (state, view) => {
    console.log('M_ADD_CACHED_VIEW', view)
    if (state.cachedViews.includes(view)) return
    state.cachedViews.push(view)
  },
  M_DEL_CACHED_VIEW: (state, view) => {
    console.log('M_DEL_CACHED_VIEW', view)
    const index = state.cachedViews.indexOf(view)
    index > -1 && state.cachedViews.splice(index, 1)
  },
  M_RESET_CACHED_VIEW: (state) => {
    state.cachedViews = []
  },
  push_loading_view: (state, view) => {
    state.loadingStack += 1
  },
  pop_loading_view: (state, view) => {
    setTimeout(function () {
      state.loadingStack -= 1
      if (state.loadingStack < 0) state.loadingStack = 0
    }, 300)
  }
}
const actions = {
  A_sidebar_opened({ commit }, data) {
    commit('M_sidebar_opened', data)
  },
  pushLoadingView({ commit }, data) {
    commit('push_loading_view')
  },
  popLoadingView({ commit }, data) {
    commit('pop_loading_view')
  }
}

const getters = {
  sidebar: (state) => state.sidebar,
  settings: (state) => state.settings,
  loadingStack: (state) => state.loadingStack
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

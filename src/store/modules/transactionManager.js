import { getTransactionReq } from '@/api/transactionApi'

const state = {
  dataPaged: []
}

const mutations = {
  SET_DATA_PAGED: (state, data) => {
    state.dataPaged = data
  }
}

const actions = {
  loadData({ commit }, payload) {
    // console.log()
    getTransactionReq(payload)
      .then((res) => {
        commit('SET_DATA_PAGED', res.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }
}

const getters = {
  dataPaged: (state) => state.dataPaged
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

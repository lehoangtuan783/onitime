import {
  getPartnersReq,
  getPartnerReq,
  addPartnerReq,
  editPartnerReq,
  deletePartnerReq
  // searchDataReq
} from '@/api/partnerApi'

const state = {
  partners: [],
  partnerFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_PARTNERS: (state, data) => {
    state.partners = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.partnerFilters = data
  }
}

const actions = {
  loadPartners({ state, commit }) {
    return new Promise((resolve, reject) => {
      getPartnersReq({ ...state.partnerFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_PARTNERS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadPartners')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadPartners')
  },
  changePartnerFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadPartner({ commit }, id) {
    return new Promise((resolve, reject) => {
      getPartnerReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addPartner({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addPartnerReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editPartner({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editPartnerReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deletePartner({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deletePartnerReq(id)
        .then((res) => {
          dispatch('loadPartners')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  partners: (state) => state.partners,
  partnerFilters: (state) => state.partnerFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

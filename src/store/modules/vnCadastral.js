import { getProvinceReq, getDistrictReq, getWardReq } from '@/api/vnCadastralApi'

const state = {}

const mutations = {}

const actions = {
  loadProvince({ commit }, payload) {
    return new Promise((resolve, reject) => {
      getProvinceReq(payload)
        .then((res) => {
          commit('SET_DATA_PAGED', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  loadDistrict({ commit }, payload) {
    return new Promise((resolve, reject) => {
      getDistrictReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  loadWard({ commit }, payload) {
    return new Promise((resolve, reject) => {
      getWardReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

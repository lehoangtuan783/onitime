import {
  getSupportPagedReq,
  getSupportReq,
  addSupportReq,
  editSupportReq,
  deleteSupportReq
  // searchDataReq
} from '@/api/supportApi'

const state = {
  supports: [],
  supportFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_SUPPORTS: (state, data) => {
    state.supports = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.supportFilters = data
  }
}

const actions = {
  loadSupports({ state, commit }) {
    return new Promise((resolve, reject) => {
      getSupportPagedReq({ ...state.supportFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_SUPPORTS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadSupports')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadSupports')
  },
  changeSupportFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadSupport({ commit }, id) {
    return new Promise((resolve, reject) => {
      getSupportReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addSupport({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addSupportReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editSupport({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editSupportReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteSupport({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteSupportReq(id)
        .then((res) => {
          dispatch('loadSupports')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  supports: (state) => state.supports,
  supportFilters: (state) => state.supportFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

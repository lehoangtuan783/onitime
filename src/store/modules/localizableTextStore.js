import {
  getLocalizableTextPagedReq,
  getLocalizableTextReq,
  addLocalizableTextReq,
  editLocalizableTextReq,
  deleteLocalizableTextReq
  // searchDataReq
} from '@/api/localizableTextApi'

const state = {
  localizableTexts: [],
  localizableTextFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_LOCALIZABLE_TEXTS: (state, data) => {
    state.localizableTexts = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.localizableTextFilters = data
  }
}

const actions = {
  loadLocalizableTexts({ state, commit }) {
    return new Promise((resolve, reject) => {
      getLocalizableTextPagedReq({ ...state.localizableTextFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_LOCALIZABLE_TEXTS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadLocalizableTexts')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadLocalizableTexts')
  },
  changeLocalizableTextFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadLocalizableText({ commit }, id) {
    return new Promise((resolve, reject) => {
      getLocalizableTextReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addLocalizableText({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addLocalizableTextReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editLocalizableText({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editLocalizableTextReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteLocalizableText({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteLocalizableTextReq(id)
        .then((res) => {
          dispatch('loadLocalizableTexts')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  localizableTexts: (state) => state.localizableTexts,
  localizableTextFilters: (state) => state.localizableTextFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import {
  getRoutePagedReq,
  getRouteReq,
  addRouteReq,
  editRouteReq,
  deleteRouteReq
  // searchDataReq
} from '@/api/routeApi'

const state = {
  routes: [],
  routeFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_ROUTES: (state, data) => {
    state.routes = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.routeFilters = data
  }
}

const actions = {
  loadRoutes({ state, commit }) {
    return new Promise((resolve, reject) => {
      getRoutePagedReq({ ...state.routeFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_ROUTES', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadRoutes')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadRoutes')
  },
  changeRouteFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadRoute({ commit }, id) {
    return new Promise((resolve, reject) => {
      getRouteReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addRoute({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addRouteReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editRoute({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editRouteReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteRoute({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteRouteReq(id)
        .then((res) => {
          dispatch('loadRoutes')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  routes: (state) => state.routes,
  routeFilters: (state) => state.routeFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

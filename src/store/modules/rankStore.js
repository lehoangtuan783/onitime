import {
  getRankPagedReq,
  getRankReq,
  addRankReq,
  editRankReq,
  deleteRankReq
  // searchDataReq
} from '@/api/rankApi'

const state = {
  ranks: [],
  rankFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_RANKS: (state, data) => {
    state.ranks = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.rankFilters = data
  }
}

const actions = {
  loadRanks({ state, commit }) {
    return new Promise((resolve, reject) => {
      getRankPagedReq({ ...state.rankFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_RANKS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadRanks')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadRanks')
  },
  changeRankFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadRank({ commit }, id) {
    return new Promise((resolve, reject) => {
      getRankReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addRank({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addRankReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editRank({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editRankReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteRank({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteRankReq(id)
        .then((res) => {
          dispatch('loadRanks')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  ranks: (state) => state.ranks,
  rankFilters: (state) => state.rankFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

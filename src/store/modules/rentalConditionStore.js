import {
  getRentalConditionPagedReq,
  getRentalConditionReq,
  addRentalConditionReq,
  editRentalConditionReq,
  deleteRentalConditionReq
  // searchDataReq
} from '@/api/rentalConditionApi'

const state = {
  rentalConditions: [],
  rentalConditionFilters: {
    section: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_RENTAL_CONDITIONS: (state, data) => {
    state.rentalConditions = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.rentalConditionFilters = data
  }
}

const actions = {
  loadRentalConditions({ state, commit }) {
    return new Promise((resolve, reject) => {
      getRentalConditionPagedReq({
        ...state.rentalConditionFilters,
        pageSize: state.pageSize,
        pageNumber: state.pageNumber
      })
        .then((res) => {
          console.log(res.data)
          commit('SET_RENTAL_CONDITIONS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadRentalConditions')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadRentalConditions')
  },
  changeRentalConditionFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadRentalCondition({ commit }, id) {
    return new Promise((resolve, reject) => {
      getRentalConditionReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addRentalCondition({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addRentalConditionReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editRentalCondition({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editRentalConditionReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteRentalCondition({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteRentalConditionReq(id)
        .then((res) => {
          dispatch('loadRentalConditions')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  searchData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      searchDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  rentalConditions: (state) => state.rentalConditions,
  rentalConditionFilters: (state) => state.rentalConditionFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

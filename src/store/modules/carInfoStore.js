import {
  getCarInfosReq,
  getCarInfoReq,
  addCarInfoReq,
  editCarInfoReq,
  deleteCarInfoReq
  // searchDataReq
} from '@/api/carInfoApi'

const state = {
  carInfos: [],
  carInfoFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CAR_INFOS: (state, data) => {
    state.carInfos = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.carInfoFilters = data
  }
}

const actions = {
  loadCarInfos({ state, commit }) {
    return new Promise((resolve, reject) => {
      getCarInfosReq({ ...state.carInfoFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CAR_INFOS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadCarInfos')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadCarInfos')
  },
  changeCarInfoFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadCarInfo({ commit }, id) {
    return new Promise((resolve, reject) => {
      getCarInfoReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addCarInfo({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addCarInfoReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editCarInfo({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editCarInfoReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteCarInfo({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteCarInfoReq(id)
        .then((res) => {
          dispatch('loadCarInfos')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  carInfos: (state) => state.carInfos,
  carInfoFilters: (state) => state.carInfoFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import {
  getAreaPagedReq,
  getAreaReq,
  addAreaReq,
  editAreaReq,
  deleteAreaReq
  // searchDataReq
} from '@/api/areaApi'

const state = {
  areas: [],
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_AREAS: (state, data) => {
    state.areas = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.areaFilters = data
  }
}

const actions = {
  loadAreas({ state, commit }) {
    return new Promise((resolve, reject) => {
      getAreaPagedReq({ pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          commit('SET_AREAS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadAreas')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadAreas')
  },
  changeAreaFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadArea({ commit }, id) {
    return new Promise((resolve, reject) => {
      getAreaReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addArea({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addAreaReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editArea({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editAreaReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteArea({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteAreaReq(id)
        .then((res) => {
          dispatch('loadAreas')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  searchData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      searchDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  areas: (state) => state.areas,
  areaFilters: (state) => state.areaFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import { getProfileReq, editPasswordReq, editAvatarReq } from '@/api/profileApi'

const getDefaultState = () => {
  return {
    //token: getToken(),
    data: null,
    roles: []
  }
}

const state = getDefaultState()

const mutations = {
  M_roles: (state, roles) => {
    state.roles = roles
  },
  SET_PROFILE: (state, profile) => {
    state.data = profile
  }
}

const actions = {
  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getProfileReq()
        .then((res) => {
          console.log(res)
          const { data } = res
          if (!data) {
            return reject('Verification failed, please Login again.')
          } else if (data.avatar === null) {
            data.avatar = 'src/assets/profile_default_image/20c.jfif'
          }
          //此处模拟数据
          const rolesArr = localStorage.getItem('roles')
          if (rolesArr) {
            data.roles = JSON.parse(rolesArr)
          } else {
            data.roles = ['admin']
            localStorage.setItem('roles', JSON.stringify(data.roles))
          }
          const { roles, username } = data
          // commit('M_username', username)
          commit('M_roles', roles)
          commit('SET_PROFILE', data)
          // commit('SET_AVATAR', avatar)
          resolve(data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editPassword({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      editPasswordReq(payload)
        .then((res) => {
          console.log(res)
          if (!res.status) {
            return reject('Password is not match,please try again')
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editAvatar({ commit }, payload) {
    return new Promise((resolve, reject) => {
      let formData = new FormData()
      formData.append('avatar', payload.avatar)
      editAvatarReq(formData)
        .then((res) => {
          resolve(res)
          console.log(res)
          if (!res.status) {
            return reject('Image can not be uploaded,please try again')
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  data: (state) => state.data
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import { addDataReq } from '@/api/notificationApi'

const state = {
  dataPaged: []
}

const mutations = {
  SET_DATA_PAGED: (state, data) => {
    state.dataPaged = data
  }
}

const actions = {
  addData({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addDataReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

import {
  getPriceListsReq,
  getPriceListReq,
  addPriceListReq,
  editPriceListReq,
  deletePriceListReq
  // searchDataReq
} from '@/api/priceListApi'

const state = {
  priceLists: [],
  priceListFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_PRICE_LISTS: (state, data) => {
    state.priceLists = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.priceListFilters = data
  }
}

const actions = {
  loadPriceLists({ state, commit }) {
    return new Promise((resolve, reject) => {
      getPriceListsReq({ ...state.priceListFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_PRICE_LISTS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadPriceLists')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadPriceLists')
  },
  changePriceListFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadPriceList({ commit }, id) {
    return new Promise((resolve, reject) => {
      getPriceListReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addPriceList({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addPriceListReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editPriceList({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editPriceListReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deletePriceList({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deletePriceListReq(id)
        .then((res) => {
          dispatch('loadPriceLists')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  priceLists: (state) => state.priceLists,
  priceListFilters: (state) => state.priceListFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

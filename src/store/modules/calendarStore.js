import { getBookingsReq } from '@/api/bookingApi'
import { calendarType, calendarStatus } from '@/constants/booking'

const state = {
  calendars: [],
  filters: {
    timeLeave: new Date().toLocaleDateString('vi-VN', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    }),
    request: 'booking',
    types: calendarType,
    status: calendarStatus
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CALENDARS: (state, data) => {
    state.calendars = data
  },
  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.filters = data
  }
}

const actions = {
  loadCalendars({ commit }) {
    return new Promise((resolve, reject) => {
      getBookingsReq({ ...state.filters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CALENDARS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadCalendars')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadCalendars')
  },
  changeCalendarDayFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  }
}

const getters = {
  calendars: (state) => state.calendars,
  filters: (state) => state.filters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

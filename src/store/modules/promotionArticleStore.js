import {
  getPromotionArticlesReq,
  getPromotionArticleReq,
  addPromotionArticleReq,
  editPromotionArticleReq,
  deletePromotionArticleReq
  // searchDataReq
} from '@/api/promotionArticleApi'

const state = {
  promotionArticles: [],
  promotionArticleFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_PROMOTION_ARTICLE: (state, data) => {
    state.promotionArticles = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.promotionArticleFilters = data
  }
}

const actions = {
  loadPromotionArticles({ state, commit }) {
    return new Promise((resolve, reject) => {
      getPromotionArticlesReq({
        ...state.promotionArticleFilters,
        pageSize: state.pageSize,
        pageNumber: state.pageNumber
      })
        .then((res) => {
          console.log(res.data)
          commit('SET_PROMOTION_ARTICLE', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadPromotionArticles')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadPromotionArticles')
  },
  changePromotionArticleFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadPromotionArticle({ commit }, id) {
    return new Promise((resolve, reject) => {
      getPromotionArticleReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addPromotionArticle({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addPromotionArticleReq({
        ...payload,
        imageIds: payload.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editPromotionArticle({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editPromotionArticleReq(payload.id, {
        ...payload.data,
        imageIds: payload.data.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deletePromotionArticle({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deletePromotionArticleReq(id)
        .then((res) => {
          dispatch('loadPromotionArticles')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  promotionArticles: (state) => state.promotionArticles,
  promotionArticleFilters: (state) => state.promotionArticleFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

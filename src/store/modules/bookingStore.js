import {
  getBookingsReq,
  getBookingReq,
  addBookingReq,
  editBookingReq,
  deleteBookingReq
  // searchDataReq
} from '@/api/bookingApi'

const state = {
  bookings: [],
  bookingFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_BOOKINGS: (state, data) => {
    state.bookings = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.bookingFilters = data
  }
}

const actions = {
  loadBookings({ state, commit }) {
    return new Promise((resolve, reject) => {
      getBookingsReq({ ...state.bookingFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_BOOKINGS', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadBookings')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadBookings')
  },
  changeBookingFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadBooking({ commit }, id) {
    return new Promise((resolve, reject) => {
      getBookingReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addBooking({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addBookingReq(payload)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editBooking({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editBookingReq(payload.id, payload.data)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteBooking({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteBookingReq(id)
        .then((res) => {
          dispatch('loadBookings')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  bookings: (state) => state.bookings,
  bookingFilters: (state) => state.bookingFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

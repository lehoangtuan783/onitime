import {
  getCarTypesReq,
  getCarTypeReq,
  addCarTypeReq,
  editCarTypeReq,
  deleteCarTypeReq
  // searchDataReq
} from '@/api/carTypeApi'

const state = {
  carTypes: [],
  carTypeFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_CAR_TYPES: (state, data) => {
    state.carTypes = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.carTypeFilters = data
  }
}

const actions = {
  loadCarTypes({ state, commit }) {
    return new Promise((resolve, reject) => {
      getCarTypesReq({ ...state.carTypeFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_CAR_TYPES', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadCarTypes')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadCarTypes')
  },
  changeCarTypeFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadCarType({ commit }, id) {
    return new Promise((resolve, reject) => {
      getCarTypeReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addCarType({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addCarTypeReq({
        ...payload,
        imageIds: payload.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editCarType({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editCarTypeReq(payload.id, {
        ...payload.data,
        imageIds: payload.data.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteCarType({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteCarTypeReq(id)
        .then((res) => {
          dispatch('loadCarTypes')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  carTypes: (state) => state.carTypes,
  carTypeFilters: (state) => state.carTypeFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

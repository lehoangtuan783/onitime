import {
  getNotificationInstructionsReq,
  getNotificationInstructionReq,
  addNotificationInstructionReq,
  editNotificationInstructionReq,
  deleteNotificationInstructionReq
  // searchDataReq
} from '@/api/notificationInstructionApi'

const state = {
  notificationInstructions: [],
  notificationInstructionFilters: {
    type: null
  },
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  SET_NOTIFICATION_INSTRUCTION: (state, data) => {
    state.notificationInstructions = data
  },

  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  CHANGE_FILTERS: (state, data) => {
    state.notificationInstructionFilters = data
  }
}

const actions = {
  loadNotificationInstructions({ state, commit }) {
    return new Promise((resolve, reject) => {
      getNotificationInstructionsReq({ ...state.notificationInstructionFilters, pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          console.log(res.data)
          commit('SET_NOTIFICATION_INSTRUCTION', res.data)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadNotificationInstructions')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadNotificationInstructions')
  },
  changeNotificationInstructionFilters({ state, commit, dispatch }, payload) {
    commit('CHANGE_FILTERS', payload)
  },

  loadNotificationInstruction({ commit }, id) {
    return new Promise((resolve, reject) => {
      getNotificationInstructionReq(id)
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addNotificationInstruction({ commit }, payload) {
    return new Promise((resolve, reject) => {
      addNotificationInstructionReq({
        ...payload,
        imageIds: payload.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editNotificationInstruction({ commit }, payload) {
    return new Promise((resolve, reject) => {
      editNotificationInstructionReq(payload.id, {
        ...payload.data,
        imageIds: payload.data.images.map((e) => {
          return e.id
        })
      })
        .then((res) => {
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteNotificationInstruction({ dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteNotificationInstructionReq(id)
        .then((res) => {
          dispatch('loadNotificationInstructions')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const getters = {
  notificationInstructions: (state) => state.notificationInstructions,
  notificationInstructionFilters: (state) => state.notificationInstructionFilters,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

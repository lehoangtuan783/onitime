import { getImagePageReq, addImageReq, deleteImageReq } from '@/api/imageApi'

const state = {
  dataPaged: [],
  selected: [],
  pageSize: 10,
  pageNumber: 1
}

const mutations = {
  INITIALIZE: (state, data) => {
    state.dataPaged = []
    state.selected = []
  },
  SET_DATA_PAGED: (state, data) => {
    state.dataPaged = data
  },
  CHANGE_SIZE: (state, data) => {
    state.pageSize = data
  },
  CHANGE_NUMBER: (state, data) => {
    state.pageNumber = data
  },
  SELECTED_IMAGE: (state, data) => {
    console.log(data)
    const index = state.selected.filter((e) => e.id == data.id).length
    if (index > 0) {
      state.selected = state.selected.filter((e) => e.id != data.id) // 2nd parameter means remove one item only
    } else {
      state.selected.push(data)
    }
  }
}

const actions = {
  initial({ state, commit, dispatch }, payload) {
    commit('INITIALIZE')
    dispatch('loadPaged')
  },
  loadPaged({ state, commit }) {
    return new Promise((resolve, reject) => {
      getImagePageReq({ pageSize: state.pageSize, pageNumber: state.pageNumber })
        .then((res) => {
          commit('SET_DATA_PAGED', res.data)
          console.log(state.selected)
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  changeSize({ state, commit, dispatch }, payload) {
    commit('CHANGE_SIZE', payload)
    dispatch('loadPaged')
  },
  changeNumber({ state, commit, dispatch }, payload) {
    commit('CHANGE_NUMBER', payload)
    dispatch('loadPaged')
  },
  selectedImage({ state, commit, dispatch }, payload) {
    commit('SELECTED_IMAGE', payload)
  },
  remoteAddImage({ state, commit, dispatch }, payload) {
    // console.log(payload)
    return new Promise((resolve, reject) => {
      let formData = new FormData()
      formData.append('image', payload.image)
      addImageReq(formData)
        .then((res) => {
          dispatch('loadPaged')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteImage({ state, commit, dispatch }, id) {
    return new Promise((resolve, reject) => {
      deleteImageReq(id)
        .then((res) => {
          dispatch('loadPaged')
          resolve(res)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
  // addData({ commit }, payload) {
  //   return new Promise((resolve, reject) => {
  //     addDataReq(payload)
  //       .then((res) => {
  //         resolve(res)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // },
  // editData({ commit }, payload) {
  //   return new Promise((resolve, reject) => {
  //     editDataReq(payload)
  //       .then((res) => {
  //         resolve(res)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // },
  // deleteData({ commit }, payload) {
  //   return new Promise((resolve, reject) => {
  //     deleteDataReq(payload)
  //       .then((res) => {
  //         resolve(res)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // },
  // searchData({ commit }, payload) {
  //   return new Promise((resolve, reject) => {
  //     searchDataReq(payload)
  //       .then((res) => {
  //         resolve(res)
  //       })
  //       .catch((error) => {
  //         reject(error)
  //       })
  //   })
  // }
}

const getters = {
  dataPaged: (state) => state.dataPaged,
  selected: (state) => state.selected,
  pageSize: (state) => state.pageSize,
  pageNumber: (state) => state.pageNumber
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

<template>
  <manager-template-page :title="$t($route.meta.title)">
    <div class="box-card">
      <el-form
        ref="form"
        :model="form"
        status-icon
        :rules="rules"
        label-width="auto"
        class="demo-form"
        label-position="top"
      >
        <el-form-item :label="$t('priceList.form.type')" prop="type">
          <el-select v-model="form.type" placeholder="">
            <el-option
              v-for="item in priceListCarType"
              :key="item.value"
              :label="$t('carType.type.' + item)"
              :value="item"
            ></el-option>
          </el-select>
        </el-form-item>
        <el-form-item :label="$t('priceList.form.province')" prop="province">
          <el-input v-model="form.province" type="text" autocomplete="off" />
        </el-form-item>
        <el-form-item :label="$t('priceList.form.seat')" prop="seat">
          <el-input v-model="form.seat" type="number" autocomplete="off" />
        </el-form-item>
        <el-form-item :label="$t('priceList.form.routeName')" prop="routeName">
          <el-input v-model="form.routeName" type="text" autocomplete="off" />
        </el-form-item>
        <el-form-item :label="$t('priceList.form.priceRange')" prop="priceRange">
          <el-input v-model="form.priceRange" type="text" autocomplete="off" />
        </el-form-item>

        <el-form-item>
          <el-button type="primary" @click="submitForm('form')">
            {{ $t($route.meta.title) }}
          </el-button>
        </el-form-item>
      </el-form>
    </div>
    <el-dialog v-if="imageGalleryVisible" v-model="imageGalleryVisible" width="80%">
      <template #title>
        <b>{{ $t('priceList.manager.delete.title') }}</b>
      </template>
      <image-gallery @onImageSubmitted="onImageSubmitted"></image-gallery>
    </el-dialog>
  </manager-template-page>
</template>

<script>
import { ElMessage } from 'element-plus'
import { mapGetters, mapActions } from 'vuex'
import { priceListCarType } from '@/constants/carType'

export default {
  data() {
    return {
      priceListCarType: priceListCarType,
      form: {
        type: '',
        province: '',
        seat: '',
        routeName: '',
        priceRange: ''
      }
    }
  },
  computed: {
    rules() {
      return {
        value: [{ required: true, message: this.$t('priceList.form.valueRequired'), trigger: 'blur' }],
        quantity: [{ required: true, message: this.$t('priceList.form.quantityRequired'), trigger: 'blur' }],
        remaining: [{ required: true, message: this.$t('priceList.form.remainingRequired'), trigger: 'blur' }],
        perUser: [{ required: true, message: this.$t('priceList.form.perUserRequired'), trigger: 'blur' }],
        startDate: [{ required: true, message: this.$t('priceList.form.startDateRequired'), trigger: 'blur' }],
        endDate: [{ required: true, message: this.$t('priceList.form.endDateRequired'), trigger: 'blur' }]
      }
    }
  },
  mounted() {
    if (this.$route.params.priceListId) {
      this.loadPriceList(this.$route.params.priceListId).then((res) => {
        this.form = res.data
      })
    }
  },
  methods: {
    ...mapActions('priceListStore', ['loadPriceList', 'addPriceList', 'editPriceList']),
    submitAdd() {
      this.addPriceList(this.form).then((res) => {
        ElMessage({ message: res.messages.join(), type: 'success' })
        this.$router.replace({ name: 'PriceListManager' })
      })
    },
    submitEdit() {
      this.editPriceList({ id: this.$route.params.priceListId, data: this.form }).then((res) => {
        ElMessage({ message: res.messages.join(), type: 'success' })
        this.$router.replace({ name: 'PriceListManager' })
      })
    },
    submitForm(formEl) {
      console.log(this.time)
      if (!formEl) return
      this.$refs[formEl].validate((valid) => {
        if (valid) {
          if (this.$route.params.priceListId) {
            this.submitEdit()
          } else {
            this.submitAdd()
          }
        } else {
          console.log('error submit!')
          return false
        }
      })
    }
  }
}
</script>

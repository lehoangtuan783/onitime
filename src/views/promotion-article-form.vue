<template>
  <manager-template-page :title="$t($route.meta.title)">
    <div class="box-card">
      <el-form
        ref="form"
        :model="form"
        status-icon
        :rules="rules"
        label-width="auto"
        class="demo-form"
        label-position="top"
      >
        <el-form-item :label="$t('promotionArticle.form.image')" prop="image">
          <el-button class="filter-item" type="success" @click="imageGalleryVisible = true">
            <el-icon><svg-icon icon-class="add" fill="#FFFFFF" /></el-icon>
          </el-button>
          <div class="image-selected-container">
            <div v-for="(item, index) in form.image" :key="index">
              <img class="image-selected" :src="item.path" alt="" />
            </div>
          </div>
        </el-form-item>
        <el-form-item :label="$t('promotionArticle.form.title')" prop="title">
          <el-input v-model="form.title" :rows="10" type="textarea" autocomplete="off" />
        </el-form-item>
        <el-form-item :label="$t('promotionArticle.form.content')" prop="content">
          <el-input v-model="form.content" :rows="10" type="textarea" autocomplete="off" />
        </el-form-item>

        <el-form-item>
          <el-button type="primary" @click="submitForm('form')">
            {{ $t($route.meta.title) }}
          </el-button>
        </el-form-item>
      </el-form>
    </div>
    <el-dialog v-if="imageGalleryVisible" v-model="imageGalleryVisible" width="80%">
      <template #title>
        <b>{{ $t($route.meta.title) }}</b>
      </template>
      <image-gallery @onImageSubmitted="onImageSubmitted"></image-gallery>
    </el-dialog>
  </manager-template-page>
</template>

<script>
import { ElMessage } from 'element-plus'
import { mapGetters, mapActions } from 'vuex'
import { getPromotionArticleReq, addPromotionArticleReq, editPromotionArticleReq } from '@/api/promotionArticleApi'

export default {
  data() {
    return {
      imageGalleryVisible: false,
      form: {
        image: null,
        title: '',
        content: ''
      }
    }
  },
  computed: {
    rules() {
      return {
        image: [{ required: true, message: this.$t('promotionArticle.form.imageRequired'), trigger: 'blur' }],
        title: [{ required: true, message: this.$t('promotionArticle.form.titleRequired'), trigger: 'blur' }],
        content: [{ required: true, message: this.$t('promotionArticle.form.contentRequired'), trigger: 'blur' }]
      }
    }
  },
  mounted() {
    console.log(this.$route)
    if (this.$route.params.promotionArticleId) {
      this.loadPromotionArticle(this.$route.params.promotionArticleId).then((res) => {
        this.form = res.data
      })
    }
  },
  methods: {
    ...mapActions('promotionArticleStore', ['loadPromotionArticle', 'addPromotionArticle', 'editPromotionArticle']),
    onImageSubmitted(val) {
      this.form.image = val
      this.imageGalleryVisible = false
    },
    submitForm(formEl) {
      if (!formEl) return
      this.$refs[formEl].validate((valid) => {
        if (valid) {
          if (this.$route.params.promotionArticleId) {
            editPromotionArticleReq(this.$route.params.promotionArticleId, {
              imageId: this.form.image[0].id,
              title: this.form.title,
              content: this.form.content
            }).then((res) => {
              ElMessage({ message: res.messages.join(), type: 'success' })
              this.$router.replace({ name: 'PromotionArticleManager' })
            })
          } else {
            addPromotionArticleReq({
              imageId: this.form.image[0].id,
              title: this.form.title,
              content: this.form.content
            }).then((res) => {
              ElMessage({ message: res.messages.join(), type: 'success' })
              this.$router.replace({ name: 'PromotionArticleManager' })
            })
          }
        } else {
          console.log('error submit!')
          return false
        }
      })
    }
  }
}
</script>

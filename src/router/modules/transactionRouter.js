import Layout from '@/layout'

const transactionManagerpager = {
  path: '/',
  component: Layout,
  redirect: '/transaction-manager',
  name: 'Transaction',
  meta: {
    title: 'page.transaction',
    icon: 'transaction'
  },
  children: [
    {
      path: '/transaction-manager',
      name: 'TransactionManager',
      component: () => import('@/views/transaction/manager/index.vue'),
      meta: { title: 'page.transactionManager', icon: 'transaction', affix: true }
    },
    {
      path: '/transaction-manager/form',
      name: 'TransactionForm',
      hidden: true,
      component: () => import('@/views/transaction/form/index.vue'),
      meta: { title: 'page.transactionAdd', icon: 'transaction', affix: true }
    }
  ]
}

export default transactionManagerpager

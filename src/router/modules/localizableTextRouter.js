import Layout from '@/layout'

const localizableTextManager = {
  path: '/localizabletextmanager',
  component: Layout,
  redirect: '/localizabletextmanager',
  name: 'LocalizableText',
  meta: {
    title: 'page.localizableText.title',
    icon: 'localizableText'
  },
  children: [
    {
      path: '',
      name: 'LocalizableTextManager',
      component: () => import('@/views/localizable-text-manager.vue'),
      meta: { title: 'page.localizableText.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'LocalizableTextAdd',
      component: () => import('@/views/localizable-text-form.vue'),
      meta: { title: 'page.localizableText.add', icon: '', affix: true }
    },
    {
      path: 'form/:localizableTextId',
      name: 'LocalizableTextEdit',
      hidden: true,
      component: () => import('@/views/localizable-text-form.vue'),
      meta: { title: 'page.localizableText.edit', icon: '', affix: true }
    }
  ]
}

export default localizableTextManager

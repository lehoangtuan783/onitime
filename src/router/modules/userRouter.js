import Layout from '@/layout'

const userManager = {
  path: '/usermanager',
  component: Layout,
  redirect: '/usermanager',
  name: 'User',
  meta: {
    title: 'page.user.title',
    icon: 'user'
  },
  children: [
    {
      path: '',
      name: 'UserManager',
      component: () => import('@/views/user-manager.vue'),
      meta: { title: 'page.user.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'UserAdd',
      component: () => import('@/views/user-add.vue'),
      meta: { title: 'page.user.add', icon: '', affix: true }
    },
    {
      path: 'form/:userId',
      name: 'UserEdit',
      hidden: true,
      component: () => import('@/views/user-edit.vue'),
      meta: { title: 'page.user.edit', icon: '', affix: true }
    }
  ]
}

export default userManager

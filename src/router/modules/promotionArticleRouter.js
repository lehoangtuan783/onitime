import Layout from '@/layout'

const promotionArticleManager = {
  path: '/promotionarticlemanager',
  component: Layout,
  redirect: '/promotionarticlemanager',
  name: 'PromotionArticle',
  meta: {
    title: 'page.promotionArticle.title',
    icon: 'promotion-article'
  },
  children: [
    {
      path: '',
      name: 'PromotionArticleManager',
      component: () => import('@/views/promotion-article-manager.vue'),
      meta: { title: 'page.promotionArticle.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'PromotionArticleAdd',
      component: () => import('@/views/promotion-article-form.vue'),
      meta: { title: 'page.promotionArticle.add', icon: '', affix: true }
    },
    {
      path: 'form/:promotionArticleId',
      name: 'PromotionArticleEdit',
      hidden: true,
      component: () => import('@/views/promotion-article-form.vue'),
      meta: { title: 'page.promotionArticle.edit', icon: '', affix: true }
    }
  ]
}

export default promotionArticleManager

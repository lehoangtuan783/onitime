import Layout from '@/layout'

const partnerManager = {
  path: '/partnermanager',
  component: Layout,
  redirect: '/partnermanager',
  name: 'Partner',
  meta: {
    title: 'page.partner.title',
    icon: 'partner'
  },
  children: [
    {
      path: '',
      name: 'PartnerManager',
      component: () => import('@/views/partner-manager.vue'),
      meta: { title: 'page.partner.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'PartnerAdd',
      component: () => import('@/views/partner-form.vue'),
      meta: { title: 'page.partner.add', icon: '', affix: true }
    },
    {
      path: 'form/:partnerId',
      name: 'PartnerEdit',
      hidden: true,
      component: () => import('@/views/partner-form.vue'),
      meta: { title: 'page.partner.edit', icon: '', affix: true }
    }
  ]
}

export default partnerManager

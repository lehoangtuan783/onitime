import Layout from '@/layout'

const configManager = {
  path: '/configmanager',
  component: Layout,
  redirect: '/configmanager',
  name: 'Config',
  meta: {
    title: 'page.config.title',
    icon: 'config'
  },
  children: [
    {
      path: '',
      name: 'ConfigManager',
      component: () => import('@/views/config-manager.vue'),
      meta: { title: 'page.config.manager', icon: 'config', affix: true }
    },
    // {
    //   path: 'form',
    //   name: 'ConfigAdd',
    //   component: () => import('@/views/config-form.vue'),
    //   meta: { title: 'page.config.add', icon: '', affix: true }
    // },
    {
      path: 'form/:configId',
      name: 'ConfigEdit',
      hidden: true,
      component: () => import('@/views/config-form.vue'),
      meta: { title: 'page.config.edit', icon: '', affix: true }
    }
  ]
}

export default configManager

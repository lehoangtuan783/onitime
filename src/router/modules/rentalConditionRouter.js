import Layout from '@/layout'

const rentalConditionManager = {
  path: '/rentalconditionmanager',
  component: Layout,
  redirect: '/rentalconditionmanager',
  name: 'RentalCondition',
  meta: {
    title: 'page.rentalCondition.title',
    icon: 'rental-condition'
  },
  children: [
    {
      path: '',
      name: 'RentalConditionManager',
      component: () => import('@/views/rental-condition-manager.vue'),
      meta: { title: 'page.rentalCondition.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'RentalConditionAdd',
      component: () => import('@/views/rental-condition-form.vue'),
      meta: { title: 'page.rentalCondition.add', icon: '', affix: true }
    },
    {
      path: 'form/:rentalConditionId',
      name: 'RentalConditionEdit',
      hidden: true,
      component: () => import('@/views/rental-condition-form.vue'),
      meta: { title: 'page.rentalCondition.edit', icon: '', affix: true }
    }
  ]
}

export default rentalConditionManager

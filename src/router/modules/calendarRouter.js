import Layout from '@/layout'

const calendarRouter = {
  path: '/calendar',
  component: Layout,
  redirect: '/calendar',
  name: 'Calendar',
  meta: {
    title: 'page.calendar.title',
    icon: 'calendar'
  },
  children: [
    {
      path: '/calendar',
      name: 'Calendar',
      component: () => import('@/views/calendar.vue'),
      meta: { title: 'page.calendar.day', icon: '', affix: true }
    },
    {
      hidden: true,
      path: '/calendar/:calendarId',
      name: 'CalendarEdit',
      component: () => import('@/views/calendar-edit.vue'),
      meta: { title: 'page.calendar.edit', icon: '', affix: true }
    },
    {
      path: '/asking',
      name: 'Asking',
      component: () => import('@/views/calendar-asking.vue'),
      meta: { title: 'page.calendar.asking', icon: '', affix: true }
    },
    {
      path: '/rental',
      name: 'rental',
      component: () => import('@/views/calendar-rental.vue'),
      meta: { title: 'page.calendar.rental', icon: '', affix: true }
    }
    // {
    //   path: '/calendar-detail',
    //   name: 'CalendarDetail',
    //   component: () => import('@/views/calendar-detail/index.vue'),
    //   meta: { title: 'page.calendarDetail', icon: '', affix: true },
    //   hidden: true
    // }
  ]
}

export default calendarRouter

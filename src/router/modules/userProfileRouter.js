import Layout from '@/layout'
const userProfileRouter = {
  path: '/userprofile',

  component: Layout,
  hidden: true,
  name: 'UserProfile',
  children: [{ path: '', component: () => import('@/views/user-profile.vue') }]
}
export default userProfileRouter

import Layout from '@/layout'

const carInfoManager = {
  path: '/carinfomanager',
  component: Layout,
  redirect: '/carinfomanager',
  name: 'CarInfo',
  meta: {
    title: 'page.carInfo.title',
    icon: 'car-info'
  },
  children: [
    {
      path: '',
      name: 'CarInfoManager',
      component: () => import('@/views/car-info-manager.vue'),
      meta: { title: 'page.carInfo.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'CarInfoAdd',
      component: () => import('@/views/car-info-form.vue'),
      meta: { title: 'page.carInfo.add', icon: '', affix: true }
    },
    {
      path: 'form/:carInfoId',
      name: 'CarInfoEdit',
      hidden: true,
      component: () => import('@/views/car-info-form.vue'),
      meta: { title: 'page.carInfo.edit', icon: '', affix: true }
    }
  ]
}

export default carInfoManager

import Layout from '@/layout'

const debug = {
  path: '/debug',
  component: Layout,
  redirect: 'debug/imagemanager',
  name: 'Debug',
  meta: {
    title: 'page.debug.title',
    icon: 'debug'
  },
  children: [
    {
      path: 'imagemanager',
      name: 'ImageManager',
      component: () => import('@/views/image-manager.vue'),
      meta: { title: 'page.debug.imageManager', icon: '', affix: true }
    },
    {
      path: 'walletransactionadd',
      name: 'WalletTransactionAdd',
      component: () => import('@/views/wallet-transaction-add.vue'),
      meta: { title: 'page.debug.walletTransaction.add', icon: '', affix: true }
    }
    // {
    //   path: 'googlemanager',
    //   name: 'GoogleManager',
    //   component: () => import('@/views/google-manager.vue'),
    //   meta: { title: 'page.debug.googleManager', icon: '', affix: true }
    // }
    // {
    //   path: 'form',
    //   name: 'CarTypeAdd',
    //   component: () => import('@/views/car-type-form.vue'),
    //   meta: { title: 'page.debug.add', icon: '', affix: true }
    // },
    // {
    //   path: 'form/:debugId',
    //   name: 'CarTypeEdit',
    //   hidden: true,
    //   component: () => import('@/views/car-type-form.vue'),
    //   meta: { title: 'page.debug.edit', icon: '', affix: true }
    // }
  ]
}

export default debug

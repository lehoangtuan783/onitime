import Layout from '@/layout'

const staticsticManagerRouter = {
  path: '/',
  component: Layout,
  redirect: '/staticstic-manager',
  name: 'staticstic',
  meta: {
    title: 'page.statistic',
    icon: 'statistic'
  },
  children: [
    {
      path: '/statistic-general',
      name: 'statisticGeneral',
      component: () => import('@/views/statistic/statistic-general.vue'),
      meta: { title: 'page.statisticGeneral', affix: true }
    },
    {
      path: '/statistic-vat',
      name: 'statisticVat',
      component: () => import('@/views/statistic/statistic-vat.vue'),
      meta: { title: 'page.statisticVat', affix: true }
    },
    {
      path: '/statistic-debt-customer',
      name: 'statisticDebtCustomer',
      component: () => import('@/views/statistic/statistic-debt-customer.vue'),
      meta: { title: 'page.statisticDebtCustomer', affix: true }
    }
  ]
}

export default staticsticManagerRouter

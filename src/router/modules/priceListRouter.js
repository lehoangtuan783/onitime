import Layout from '@/layout'

const priceListManager = {
  path: '/pricelistmanager',
  component: Layout,
  redirect: '/pricelistmanager',
  name: 'PriceList',
  meta: {
    title: 'page.priceList.title',
    icon: 'price-list'
  },
  children: [
    {
      path: '',
      name: 'PriceListManager',
      component: () => import('@/views/price-list-manager.vue'),
      meta: { title: 'page.priceList.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'PriceListAdd',
      component: () => import('@/views/price-list-form.vue'),
      meta: { title: 'page.priceList.add', icon: '', affix: true }
    },
    {
      path: 'form/:priceListId',
      name: 'PriceListEdit',
      hidden: true,
      component: () => import('@/views/price-list-form.vue'),
      meta: { title: 'page.priceList.edit', icon: '', affix: true }
    }
  ]
}

export default priceListManager

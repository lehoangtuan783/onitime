import Layout from '@/layout'

const supportManager = {
  path: '/supportmanager',
  component: Layout,
  redirect: '/supportmanager',
  name: 'Support',
  meta: {
    title: 'page.support.title',
    icon: 'support'
  },
  children: [
    {
      path: '',
      name: 'SupportManager',
      component: () => import('@/views/support-manager.vue'),
      meta: { title: 'page.support.manager', icon: 'support', affix: true }
    },
    // {
    //   path: 'form',
    //   name: 'SupportAdd',
    //   component: () => import('@/views/support-form.vue'),
    //   meta: { title: 'page.support.add', icon: '', affix: true }
    // },
    {
      path: 'form/:supportId',
      name: 'SupportEdit',
      hidden: true,
      component: () => import('@/views/support-form.vue'),
      meta: { title: 'page.support.edit', icon: '', affix: true }
    }
  ]
}

export default supportManager

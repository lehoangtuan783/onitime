import Layout from '@/layout'

const notificationInstructionRouter = {
  path: '/notificationInstructionmanager',
  component: Layout,
  redirect: '/NotificationInstructionmanager',
  name: 'NotificationInstruction',
  meta: {
    title: 'page.notificationInstruction.title',
    icon: 'notification-instruction'
  },
  children: [
    {
      path: '',
      name: 'NotificationInstructionManager',
      component: () => import('@/views/notification-instruction-manager.vue'),
      meta: { title: 'page.notificationInstruction.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'NotificationInstructionAdd',
      component: () => import('@/views/notification-instruction-form.vue'),
      meta: { title: 'page.notificationInstruction.add', icon: '', affix: true }
    },
    {
      path: 'form/:notificationInstructionId',
      name: 'NotificationInstructionEdit',
      hidden: true,
      component: () => import('@/views/notification-instruction-form.vue'),
      meta: { title: 'page.notificationInstruction.edit', icon: '', affix: true }
    }
  ]
}

export default notificationInstructionRouter

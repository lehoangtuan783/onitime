import Layout from '@/layout'

const bankAccountManager = {
  path: '/bankaccountmanager',
  component: Layout,
  redirect: '/bankaccountmanager',
  name: 'BankAccount',
  meta: {
    title: 'page.bankAccount.title',
    icon: 'bank-account'
  },
  children: [
    {
      path: '',
      name: 'BankAccountManager',
      component: () => import('@/views/bank-account-manager.vue'),
      meta: { title: 'page.bankAccount.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'BankAccountAdd',
      component: () => import('@/views/bank-account-form.vue'),
      meta: { title: 'page.bankAccount.add', icon: '', affix: true }
    },
    {
      path: 'form/:bankAccountId',
      name: 'BankAccountEdit',
      hidden: true,
      component: () => import('@/views/bank-account-form.vue'),
      meta: { title: 'page.bankAccount.edit', icon: '', affix: true }
    }
  ]
}

export default bankAccountManager

import Layout from '@/layout'

const locationManager = {
  path: '/locationmanager',
  component: Layout,
  redirect: '/locationmanager',
  name: 'Location',
  meta: {
    title: 'page.location.title',
    icon: 'location'
  },
  children: [
    {
      path: '',
      name: 'LocationManager',
      component: () => import('@/views/location-manager.vue'),
      meta: { title: 'page.location.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'LocationAdd',
      component: () => import('@/views/location-form.vue'),
      meta: { title: 'page.location.add', icon: '', affix: true }
    },
    {
      path: 'form/:locationId',
      name: 'LocationEdit',
      hidden: true,
      component: () => import('@/views/location-form.vue'),
      meta: { title: 'page.location.edit', icon: '', affix: true }
    }
  ]
}

export default locationManager

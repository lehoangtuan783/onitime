import Layout from '@/layout'

const sliderManager = {
  path: '/slidermanager',
  component: Layout,
  redirect: '/slidermanager',
  name: 'Slider',
  meta: {
    title: 'page.slider.title',
    icon: 'slider'
  },
  children: [
    {
      path: '',
      name: 'SliderManager',
      component: () => import('@/views/slider-manager.vue'),
      meta: { title: 'page.slider.manager', icon: 'slider', affix: true }
    },
    // {
    //   path: 'form',
    //   name: 'SliderAdd',
    //   hidden: true,
    //   component: () => import('@/views/slider-form.vue'),
    //   meta: { title: 'page.slider.add', icon: '', affix: true }
    // },
    {
      path: 'form/:sliderId',
      name: 'SliderEdit',
      hidden: true,
      component: () => import('@/views/slider-form.vue'),
      meta: { title: 'page.slider.edit', icon: '', affix: true }
    }
  ]
}

export default sliderManager

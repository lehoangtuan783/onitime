import Layout from '@/layout'
const changePasswordRouter = {
  path: '/changepassword',

  component: Layout,
  hidden: true,
  name: 'ChangePassword',
  children: [{ path: '', component: () => import('@/views/change-password.vue') }]
}
export default changePasswordRouter

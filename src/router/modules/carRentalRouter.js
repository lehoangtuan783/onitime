import Layout from '@/layout'

const carRentalManager = {
  path: '/carrentalmanager',
  component: Layout,
  redirect: '/carrentalmanager',
  name: 'CarRental',
  meta: {
    title: 'page.carRental.title',
    icon: 'car-rental'
  },
  children: [
    {
      path: '',
      name: 'CarRentalManager',
      component: () => import('@/views/car-rental-manager.vue'),
      meta: { title: 'page.carRental.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'CarRentalAdd',
      component: () => import('@/views/car-rental-form.vue'),
      meta: { title: 'page.carRental.add', icon: '', affix: true }
    },
    {
      path: 'form/:carRentalId',
      name: 'CarRentalEdit',
      hidden: true,
      component: () => import('@/views/car-rental-form.vue'),
      meta: { title: 'page.carRental.edit', icon: '', affix: true }
    }
  ]
}

export default carRentalManager

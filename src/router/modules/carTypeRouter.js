import Layout from '@/layout'

const carTypeManager = {
  path: '/cartypemanager',
  component: Layout,
  redirect: '/cartypemanager',
  name: 'CarType',
  meta: {
    title: 'page.carType.title',
    icon: 'car-type'
  },
  children: [
    {
      path: '',
      name: 'CarTypeManager',
      component: () => import('@/views/car-type-manager.vue'),
      meta: { title: 'page.carType.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'CarTypeAdd',
      component: () => import('@/views/car-type-form.vue'),
      meta: { title: 'page.carType.add', icon: '', affix: true }
    },
    {
      path: 'form/:carTypeId',
      name: 'CarTypeEdit',
      hidden: true,
      component: () => import('@/views/car-type-form.vue'),
      meta: { title: 'page.carType.edit', icon: '', affix: true }
    }
  ]
}

export default carTypeManager

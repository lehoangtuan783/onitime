import Layout from '@/layout'

const voucherManager = {
  path: '/vouchermanager',
  component: Layout,
  redirect: '/vouchermanager',
  name: 'Voucher',
  meta: {
    title: 'page.voucher.title',
    icon: 'voucher'
  },
  children: [
    {
      path: '',
      name: 'VoucherManager',
      component: () => import('@/views/voucher-manager.vue'),
      meta: { title: 'page.voucher.manager', icon: '', affix: true }
    },
    {
      path: 'add',
      name: 'VoucherAdd',
      component: () => import('@/views/voucher-form.vue'),
      meta: { title: 'page.voucher.add', icon: '', affix: true }
    },
    {
      path: 'edit/:voucherId',
      name: 'VoucherEdit',
      hidden: true,
      component: () => import('@/views/voucher-form.vue'),
      meta: { title: 'page.voucher.edit', icon: '', affix: true }
    }
  ]
}

export default voucherManager

import Layout from '@/layout'

const areaManager = {
  path: '/areamanager',
  component: Layout,
  redirect: '/areamanager',
  name: 'Area',
  meta: {
    title: 'page.area.title',
    icon: 'area'
  },
  children: [
    {
      path: '',
      name: 'AreaManager',
      component: () => import('@/views/area-manager.vue'),
      meta: { title: 'page.area.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'AreaAdd',
      component: () => import('@/views/area-form.vue'),
      meta: { title: 'page.area.add', icon: '', affix: true }
    },
    {
      path: 'form/:areaId',
      name: 'AreaEdit',
      hidden: true,
      component: () => import('@/views/area-form.vue'),
      meta: { title: 'page.area.edit', icon: '', affix: true }
    }
  ]
}

export default areaManager

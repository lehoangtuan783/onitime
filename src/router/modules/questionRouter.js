import Layout from '@/layout'

const questionManager = {
  path: '/questionmanager',
  component: Layout,
  redirect: '/questionmanager',
  name: 'Question',
  meta: {
    title: 'page.question.title',
    icon: 'question'
  },
  children: [
    {
      path: '',
      name: 'QuestionManager',
      component: () => import('@/views/question-manager.vue'),
      meta: { title: 'page.question.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'QuestionAdd',
      component: () => import('@/views/question-form.vue'),
      meta: { title: 'page.question.add', icon: '', affix: true }
    },
    {
      path: 'form/:questionId',
      name: 'QuestionEdit',
      hidden: true,
      component: () => import('@/views/question-form.vue'),
      meta: { title: 'page.question.edit', icon: '', affix: true }
    }
  ]
}

export default questionManager

import Layout from '@/layout'

const bookingManager = {
  path: '/bookingmanager',
  component: Layout,
  redirect: '/bookingmanager',
  name: 'Booking',
  meta: {
    title: 'page.booking.title',
    icon: 'booking'
  },
  children: [
    {
      path: '',
      name: 'BookingManager',
      component: () => import('@/views/booking-manager.vue'),
      meta: { title: 'page.booking.manager', icon: '', affix: true }
    },
    {
      path: 'form',
      name: 'BookingAdd',
      component: () => import('@/views/booking-form.vue'),
      meta: { title: 'page.booking.add', icon: '', affix: true }
    },
    {
      path: 'form/:bookingId',
      name: 'BookingEdit',
      hidden: true,
      component: () => import('@/views/booking-form.vue'),
      meta: { title: 'page.booking.edit', icon: '', affix: true }
    }
  ]
}

export default bookingManager

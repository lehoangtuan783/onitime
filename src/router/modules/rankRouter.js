import Layout from '@/layout'

const rankManager = {
  path: '/rankmanager',
  component: Layout,
  redirect: '/rankmanager',
  name: 'Rank',
  meta: {
    title: 'page.rank.title',
    icon: 'rank'
  },
  children: [
    {
      path: '',
      name: 'RankManager',
      component: () => import('@/views/rank-manager.vue'),
      meta: { title: 'page.rank.manager', icon: 'rank', affix: true }
    },
    // {
    //   path: 'form',
    //   name: 'RankAdd',
    //   hidden: true,
    //   component: () => import('@/views/rank-form.vue'),
    //   meta: { title: 'page.rank.add', icon: '', affix: true }
    // },
    {
      path: 'form/:rankId',
      name: 'RankEdit',
      hidden: true,
      component: () => import('@/views/rank-form.vue'),
      meta: { title: 'page.rank.edit', icon: '', affix: true }
    }
  ]
}

export default rankManager

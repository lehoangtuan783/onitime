import Layout from '@/layout'

const routeManager = {
  path: '/routemanager',
  component: Layout,
  redirect: '/routemanager',
  name: 'Route',
  meta: {
    title: 'page.route.title',
    icon: 'route'
  },
  children: [
    {
      path: '',
      name: 'RouteManager',
      component: () => import('@/views/route-manager.vue'),
      meta: { title: 'page.route.manager', icon: '', affix: true }
    },
    {
      path: 'add',
      name: 'RouteAdd',
      component: () => import('@/views/route-add.vue'),
      meta: { title: 'page.route.add', icon: '', affix: true }
    },
    {
      path: 'edit/:routeId',
      name: 'RouteEdit',
      hidden: true,
      component: () => import('@/views/route-edit.vue'),
      meta: { title: 'page.route.edit', icon: '', affix: true }
    }
  ]
}

export default routeManager

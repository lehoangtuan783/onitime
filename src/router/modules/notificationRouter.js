import Layout from '@/layout'

const notificationRouter = {
  path: '/',
  component: Layout,
  redirect: '/notification-manager/add',
  name: 'notification',
  meta: {
    title: 'page.notification',
    icon: 'notification'
  },
  children: [
    {
      path: '/notification-manager/add',
      name: 'notificationAdd',
      component: () => import('@/views/notification-add.vue'),
      meta: { title: 'page.notification.add', icon: 'notification', affix: true }
    }
  ]
}

export default notificationRouter

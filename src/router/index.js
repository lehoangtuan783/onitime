import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import Layout from '@/layout'

import calendarRouter from './modules/calendarRouter'

import carTypeRouter from './modules/carTypeRouter'
import areaRouter from './modules/areaRouter'
import rentalConditionRouter from './modules/rentalConditionRouter'
import carRentalRouter from './modules/carRentalRouter'
import partnerRouter from './modules/partnerRouter'
import carInfoRouter from './modules/carInfoRouter'
import locationRouter from './modules/locationRouter'
import routeRouter from './modules/routeRouter'
import priceListRouter from './modules/priceListRouter'
import promotionArticleRouter from './modules/promotionArticleRouter'
import voucherRouter from './modules/voucherRouter'
import bankAccountRouter from './modules/bankAccountRouter'
import rankRouter from './modules/rankRouter'
import userRouter from './modules/userRouter'
// import bookingRouter from './modules/bookingRouter'
import supportRouter from './modules/supportRouter'
import questionRouter from './modules/questionRouter'
import notificationInstructionRouter from './modules/notificationInstructionRouter'
import sliderRouter from './modules/sliderRouter'
import configRouter from './modules/configRouter'
import debugRouter from './modules/debugRouter'
import notificationRouter from './modules/notificationRouter'
import staticsticRouter from './modules/staticsticRouter'
import changePasswordRouter from './modules/changePasswordRouter'
import userProfileRouter from './modules/userProfileRouter'

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/Login.vue'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404.vue'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401.vue'),
    hidden: true
  },
  calendarRouter,
  {
    path: '/',
    component: Layout,
    redirect: '/calendar'
  },
  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/dashboard',
  //   children: [
  //     {
  //       path: 'dashboard',
  //       name: 'Dashboard',
  //       component: () => import('@/views/dashboard/index.vue'),
  //       //using el svg icon, the elSvgIcon first when at the same time using elSvgIcon and icon
  //       meta: { title: 'page.dashboard', icon: 'dashboard' }
  //     }
  //   ]
  // },
  // {
  //   path: '/setting-switch',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/setting-switch'),
  //       name: 'SettingSwitch',
  //       meta: { title: 'Setting Switch', icon: 'example' }
  //     }
  //   ]
  // },
  // {
  //   path: '/error-log',
  //   component: Layout,
  //   name: 'ErrorLog',
  //   redirect: '/error-log/list',
  //   meta: { title: 'ErrorLog', icon: 'bug' },
  //   children: [
  //     {
  //       path: 'list',
  //       component: () => import('@/views/error-log'),
  //       name: 'ErrorLog',
  //       meta: { title: 'Error Log' }
  //     },
  //     {
  //       path: 'error-log-test',
  //       component: () => import('@/views/error-log/ErrorLogTest.vue'),
  //       name: 'ErrorLogTest',
  //       meta: { title: 'ErrorLog Test' }
  //     }
  //   ]
  // },
  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index.vue'),
  //       meta: { title: 'Form', icon: 'table' }
  //     }
  //   ]
  // },
  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index.vue'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1/index.vue'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2/index.vue'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1/index.vue'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2/index.vue'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3/index.vue'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index.vue'),
  //       name: 'Menu2',
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },
  // {
  //   path: '/external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://github.com/jzfai/vue3-admin-template.git',
  //       meta: { title: 'External Link', icon: 'link' }
  //     }
  //   ]
  // },
  // {
  //   path: '/writing-demo',
  //   component: Layout,
  //   meta: { title: 'Writing Demo', icon: 'eye-open' },
  //   alwaysShow: true,
  //   children: [
  //     {
  //       path: 'hook',
  //       component: () => import('@/views/example/hook/Hook.vue'),
  //       name: 'Hook',
  //       meta: { title: 'Hook-Demo' }
  //     },
  //     {
  //       path: 'vuex-use',
  //       component: () => import('@/views/example/vuex-use/VuexUse.vue'),
  //       name: 'VuexUse',
  //       meta: { title: 'Vuex-Demo' }
  //     },
  //     {
  //       path: 'mock-test',
  //       component: () => import('@/views/example/mock-test/MockTest.vue'),
  //       name: 'MockTest',
  //       meta: { title: 'Mock-Demo' }
  //     },
  //     {
  //       path: 'svg-icon',
  //       component: () => import('@/views/example/svg-icon/SvgIcon.vue'),
  //       name: 'SvgIcon',
  //       meta: { title: 'Svg-Demo' }
  //     },
  //     {
  //       path: 'parent-children',
  //       component: () => import('@/views/example/parent-children/Parent.vue'),
  //       name: 'Parent',
  //       meta: { title: 'Parent-Children' }
  //     },
  //     {
  //       path: 'keep-alive',
  //       component: () => import('@/views/example/keep-alive'),
  //       name: 'KeepAlive',
  //       //cachePage: cachePage when page enter, default false
  //       //leaveRmCachePage: remove cachePage when page leave, default false
  //       meta: { title: 'Keep-Alive', cachePage: true, leaveRmCachePage: false }
  //     },
  //     {
  //       path: 'router-demo-f',
  //       name: 'routerDemoF',
  //       hidden: true,
  //       component: () => import('@/views/example/keep-alive/RouterDemoF.vue'),
  //       meta: { title: 'RouterDemo-F', activeMenu: '/writing-demo/keep-alive' }
  //     },
  //     {
  //       path: 'router-demo-s',
  //       name: 'routerDemoS',
  //       hidden: true,
  //       component: () => import('@/views/example/keep-alive/RouterDemoS.vue'),
  //       meta: { title: 'RouterDemo-S', activeMenu: '/writing-demo/keep-alive' }
  //     }
  //   ]
  // },
  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index.vue'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index.vue'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },

  carTypeRouter,
  areaRouter,
  rentalConditionRouter,
  carRentalRouter,
  partnerRouter,
  carInfoRouter,
  locationRouter,
  routeRouter,
  priceListRouter,
  promotionArticleRouter,
  voucherRouter,
  bankAccountRouter,
  rankRouter,
  userRouter,
  supportRouter,
  questionRouter,
  notificationInstructionRouter,
  sliderRouter,
  configRouter,
  // bookingRouter,
  notificationRouter,
  debugRouter,
  changePasswordRouter,
  userProfileRouter
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/page',
  //   alwaysShow: true, // will always show the root menu
  //   name: 'Permission',
  //   meta: {
  //     title: 'Permission',
  //     icon: 'lock',
  //     roles: ['admin', 'editor'] // you can set roles in root nav
  //   },
  //   children: [
  //     // {
  //     //   path: 'roleIndex',
  //     //   component: () => import('@/views/permission'),
  //     //   name: 'Permission',
  //     //   meta: {
  //     //     title: 'Role Index'
  //     //     //roles: ['admin'] // or you can only set roles in sub nav
  //     //   }
  //     // },
  //     // {
  //     //   path: 'page',
  //     //   component: () => import('@/views/permission/page.vue'),
  //     //   name: 'PagePermission',
  //     //   meta: {
  //     //     title: 'Page Permission',
  //     //     roles: ['admin'] // or you can only set roles in sub nav
  //     //   }
  //     // },
  //     // {
  //     //   path: 'directive',
  //     //   component: () => import('@/views/permission/directive.vue'),
  //     //   name: 'DirectivePermission',
  //     //   meta: {
  //     //     title: 'Directive Permission'
  //     //     // if do not set roles, means: this page does not require permission
  //     //   }
  //     // },
  //     // {
  //     //   path: 'code-index',
  //     //   component: () => import('@/views/permission/CodePermission.vue'),
  //     //   name: 'CodePermission',
  //     //   meta: {
  //     //     title: 'Code Index'
  //     //   }
  //     // },
  //     // {
  //     //   path: 'code-page',
  //     //   component: () => import('@/views/permission/CodePage.vue'),
  //     //   name: 'CodePage',
  //     //   meta: {
  //     //     title: 'Code Page',
  //     //     code: 1
  //     //   }
  //     // },
  //     // 404 page must be placed at the end !!!
  //     // using pathMatch install of "*" in vue-router 4.0
  //     { path: '/:pathMatch(.*)', redirect: '/404', hidden: true }
  //   ]
  // }
]

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ top: 0 }),
  routes: constantRoutes
})

router.afterEach((to) => {
  // if (to.meta.title) {
  //   // document.title = $this.t()
  // }
  // // console.log(this.$t('route.dashboard'))
  // console.log(to.meta.title)
  // console.log(i18n.t('abc'))
  // document.title = i18n.t('abc')
})

export default router

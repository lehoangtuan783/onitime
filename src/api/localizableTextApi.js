import request from '@/utils/axiosReq'

export function getLocalizableTextPagedReq(params) {
  return request({
    url: 'localizabletextmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getLocalizableTextReq(id) {
  return request({
    url: `localizabletextmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addLocalizableTextReq(data) {
  return request({
    url: '/localizabletextmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editLocalizableTextReq(id, data) {
  return request({
    url: `localizabletextmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteLocalizableTextReq(id) {
  return request({
    url: `localizabletextmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

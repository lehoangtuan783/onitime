import request from '@/utils/axiosReq'

export function getRentalConditionPagedReq(params) {
  return request({
    url: 'rentalconditionmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getRentalConditionReq(id) {
  return request({
    url: `rentalconditionmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addRentalConditionReq(data) {
  return request({
    url: 'rentalconditionmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editRentalConditionReq(id, data) {
  return request({
    url: `rentalconditionmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteRentalConditionReq(id) {
  return request({
    url: `rentalconditionmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

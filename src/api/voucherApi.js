import request from '@/utils/axiosReq'

export function getVouchersReq(params) {
  return request({
    url: 'vouchermanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getVoucherReq(id) {
  return request({
    url: `vouchermanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addVoucherReq(data) {
  return request({
    url: '/vouchermanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editVoucherReq(id, data) {
  return request({
    url: `vouchermanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteVoucherReq(id) {
  return request({
    url: `vouchermanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

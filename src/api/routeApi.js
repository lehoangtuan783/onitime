import request from '@/utils/axiosReq'

export function getRoutePagedReq(params) {
  return request({
    url: 'routemanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getRouteReq(id) {
  return request({
    url: `routemanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addRouteReq(data) {
  return request({
    url: '/routemanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editRouteReq(id, data) {
  return request({
    url: `routemanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteRouteReq(id) {
  return request({
    url: `routemanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

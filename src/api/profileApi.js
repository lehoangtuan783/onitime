import request from '@/utils/axiosReq'

export function getProfileReq() {
  return request({
    url: 'profile',
    bfLoading: false,
    method: 'get',
    isAlertErrorMsg: false
  })
}
export function editPasswordReq(data) {
  return request({
    url: 'profile/password-change',
    data,
    bfLoading: false,
    method: 'post',
    isAlertErrorMsg: false
  })
}
export function editAvatarReq(data) {
  return request({
    url: 'profile/avatar',
    data,
    bfLoading: false,
    method: 'put',
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getCarRentalPagedReq(params) {
  return request({
    url: 'carrentalmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getCarRentalReq(id) {
  return request({
    url: `carrentalmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addCarRentalReq(data) {
  return request({
    url: 'carrentalmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editCarRentalReq(id, data) {
  return request({
    url: `carrentalmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteCarRentalReq(id) {
  return request({
    url: `carrentalmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getPartnersReq(params) {
  return request({
    url: 'partnermanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getPartnerReq(id) {
  return request({
    url: `partnermanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addPartnerReq(data) {
  return request({
    url: '/partnermanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editPartnerReq(id, data) {
  return request({
    url: `partnermanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deletePartnerReq(id) {
  return request({
    url: `partnermanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

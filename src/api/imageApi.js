import request from '@/utils/axiosReq'

export function getImagePageReq(params) {
  return request({
    url: 'appimagemanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addImageReq(data) {
  return request({
    url: 'appimagemanager',
    data,
    method: 'post',
    isUploadFile: true,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteImageReq(id) {
  return request({
    url: `appimagemanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getPriceListsReq(params) {
  return request({
    url: 'pricelistmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getPriceListReq(id) {
  return request({
    url: `pricelistmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addPriceListReq(data) {
  return request({
    url: '/pricelistmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editPriceListReq(id, data) {
  return request({
    url: `pricelistmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deletePriceListReq(id) {
  return request({
    url: `pricelistmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getPriceListOptions() {
  return request({
    url: 'pricelistmanager/options',
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

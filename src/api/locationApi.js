import request from '@/utils/axiosReq'

export function getLocationsReq(params) {
  return request({
    url: 'locationmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getLocationReq(id) {
  return request({
    url: `locationmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addLocationReq(data) {
  return request({
    url: 'locationmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editLocationReq(id, data) {
  return request({
    url: `locationmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteLocationReq(id) {
  return request({
    url: `locationmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getSupportPagedReq(params) {
  return request({
    url: 'supportmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getSupportReq(id) {
  return request({
    url: `supportmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addSupportReq(data) {
  return request({
    url: '/supportmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editSupportReq(id, data) {
  return request({
    url: `supportmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteSupportReq(id) {
  return request({
    url: `supportmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

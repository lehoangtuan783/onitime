import request from '@/utils/axiosReq'

export function getBankAccountPagedReq(params) {
  return request({
    url: 'bankaccountmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getBankAccountReq(id) {
  return request({
    url: `bankaccountmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isParams: false,
    isAlertErrorMsg: false
  })
}

export function addBankAccountReq(data) {
  return request({
    url: '/bankaccountmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editBankAccountReq(id, data) {
  return request({
    url: `bankaccountmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteBankAccountReq(id) {
  return request({
    url: `bankaccountmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

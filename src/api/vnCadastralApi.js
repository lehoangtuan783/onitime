import request from '@/utils/axiosReq'

export function getProvinceReq() {
  return request({
    url: 'vncadastralmanager/province',
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getDistrictReq(provinceId) {
  return request({
    url: `vncadastralmanager/province/${provinceId}/district`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getWardReq(districtId) {
  return request({
    url: `vncadastralmanager/district/${districtId}/ward`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

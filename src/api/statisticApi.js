import request from '@/utils/axiosReq'

export function getBookingDay(data) {
  return request({
    url: '/api/backend/statistic/booking-day',
    data,
    method: 'get',
    bfLoading: true,
    isParams: true,
    isAlertErrorMsg: false
  })
}


import request from '@/utils/axiosReq'

export function addWalletTransactionReq(data) {
  return request({
    url: 'walletmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

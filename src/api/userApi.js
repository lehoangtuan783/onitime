import request from '@/utils/axiosReq'

export function getUserPagedReq(params) {
  return request({
    url: 'usermanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getUserReq(id) {
  return request({
    url: `usermanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addUserReq(data) {
  return request({
    url: '/usermanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editUserReq(id, data) {
  return request({
    url: `usermanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteUserReq(id) {
  return request({
    url: `usermanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editUserStatusReq(data) {
  return request({
    url: `usermanager/status`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editUserRankReq(data) {
  return request({
    url: `usermanager/rank`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editUserRankPeriodReq(data) {
  return request({
    url: `usermanager/rankperiod`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

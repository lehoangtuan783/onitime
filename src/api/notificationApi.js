import request from '@/utils/axiosReq'

export function addDataReq(data) {
  return request({
    url: 'notificationmanager',
    data,
    method: 'post',
    bfLoading: true,
    isParams: false,
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getConfigPagedReq(params) {
  return request({
    url: 'configmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getConfigReq(id) {
  return request({
    url: `configmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addConfigReq(data) {
  return request({
    url: '/configmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editConfigReq(id, data) {
  return request({
    url: `configmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteConfigReq(id) {
  return request({
    url: `configmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

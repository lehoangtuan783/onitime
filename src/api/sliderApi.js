import request from '@/utils/axiosReq'

export function getSliderPagedReq(params) {
  return request({
    url: 'slidermanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getSliderReq(id) {
  return request({
    url: `slidermanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addSliderReq(data) {
  return request({
    url: '/slidermanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editSliderReq(id, data) {
  return request({
    url: `slidermanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteSliderReq(id) {
  return request({
    url: `slidermanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

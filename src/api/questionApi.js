import request from '@/utils/axiosReq'

export function getQuestionsReq(params) {
  return request({
    url: 'questionmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getQuestionReq(id) {
  return request({
    url: `questionmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addQuestionReq(data) {
  return request({
    url: '/questionmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editQuestionReq(id, data) {
  return request({
    url: `questionmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteQuestionReq(id) {
  return request({
    url: `questionmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

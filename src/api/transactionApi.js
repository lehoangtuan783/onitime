import request from '@/utils/axiosReq'

export function getTransactionReq(data) {
  return request({
    url: '/api/backend/route',
    data,
    method: 'get',
    bfLoading: true,
    isParams: true,
    isAlertErrorMsg: false
  })
}


import request from '@/utils/axiosReq'

export function getAreaPagedReq(params) {
  return request({
    url: 'areamanager',
    params,
    method: 'get',
    bfLoading: true,
    isParams: true,
    isAlertErrorMsg: false
  })
}

export function getAreaReq(id) {
  return request({
    url: `areamanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addAreaReq(data) {
  return request({
    url: 'areamanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editAreaReq(id, data) {
  return request({
    url: `areamanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteAreaReq(id) {
  return request({
    url: `areamanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

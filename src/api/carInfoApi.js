import request from '@/utils/axiosReq'

export function getCarInfosReq(params) {
  return request({
    url: 'carinfomanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getCarInfoReq(id) {
  return request({
    url: `carinfomanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addCarInfoReq(data) {
  return request({
    url: '/carinfomanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editCarInfoReq(id, data) {
  return request({
    url: `carinfomanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteCarInfoReq(id) {
  return request({
    url: `carinfomanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

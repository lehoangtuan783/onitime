import request from '@/utils/axiosReq'

export function getRankPagedReq(params) {
  return request({
    url: 'rankmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getRankReq(id) {
  return request({
    url: `rankmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addRankReq(data) {
  return request({
    url: '/rankmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editRankReq(id, data) {
  return request({
    url: `rankmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteRankReq(id) {
  return request({
    url: `rankmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

// export function addRankPeriod(data) {
//   return request({
//     url: '/api/backend/rank/rank-period',
//     method: 'post',
//     data,
//     bfLoading: true,
//     isParams: false,
//     isAlertErrorMsg: false
//   })
// }

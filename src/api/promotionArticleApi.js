import request from '@/utils/axiosReq'

export function getPromotionArticlesReq(params) {
  return request({
    url: 'promotionarticlemanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getPromotionArticleReq(id) {
  return request({
    url: `promotionarticlemanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addPromotionArticleReq(data) {
  return request({
    url: '/promotionarticlemanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editPromotionArticleReq(id, data) {
  return request({
    url: `promotionarticlemanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deletePromotionArticleReq(id) {
  return request({
    url: `promotionarticlemanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

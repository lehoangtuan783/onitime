import request from '@/utils/axiosReq'
import qs from 'qs'

export function getBookingsReq(params) {
  return request({
    url: 'bookingmanager',
    params,

    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getBookingReq(id) {
  return request({
    url: `bookingmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addBookingReq(data) {
  return request({
    url: '/bookingmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editBookingReq(id, data) {
  return request({
    url: `bookingmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteBookingReq(id) {
  return request({
    url: `bookingmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingCarInfo(id, data) {
  return request({
    url: `bookingmanager/${id}/carinfo`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingStatusReq(id, data) {
  return request({
    url: `bookingmanager/${id}/status`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingMoneyRouteReq(id, data) {
  return request({
    url: `bookingmanager/${id}/moneyroute`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingMoneyDepositedReq(id, data) {
  return request({
    url: `bookingmanager/${id}/moneydeposited`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingMoneyTransferredReq(id, data) {
  return request({
    url: `bookingmanager/${id}/moneytransferred`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putChangeBookingAddressReq(id, data) {
  return request({
    url: `bookingmanager/${id}/bookingaddress`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putChangeBookingTimeReq(id, data) {
  return request({
    url: `bookingmanager/${id}/time`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingCarTypeReq(id, data) {
  return request({
    url: `bookingmanager/${id}/cartype`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingVatStatusReq(id, data) {
  return request({
    url: `bookingmanager/${id}/vatstatus`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function putBookingRequestReq(id, data) {
  return request({
    url: `bookingmanager/${id}/request`,
    method: 'put',
    data,
    bfLoading: true,
    isAlertErrorMsg: false
  })
}


import request from '@/utils/axiosReq'

export function getCarTypesReq(params) {
  return request({
    url: 'cartypemanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getCarTypeReq(id) {
  return request({
    url: `cartypemanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addCarTypeReq(data) {
  return request({
    url: '/cartypemanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editCarTypeReq(id, data) {
  return request({
    url: `cartypemanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteCarTypeReq(id) {
  return request({
    url: `cartypemanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

import request from '@/utils/axiosReq'

export function getNotificationInstructionsReq(params) {
  return request({
    url: 'notificationinstructionmanager',
    params,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function getNotificationInstructionReq(id) {
  return request({
    url: `notificationinstructionmanager/${id}`,
    method: 'get',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function addNotificationInstructionReq(data) {
  return request({
    url: '/notificationinstructionmanager',
    data,
    method: 'post',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function editNotificationInstructionReq(id, data) {
  return request({
    url: `notificationinstructionmanager/${id}`,
    data,
    method: 'put',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

export function deleteNotificationInstructionReq(id) {
  return request({
    url: `notificationinstructionmanager/${id}`,
    method: 'delete',
    bfLoading: true,
    isAlertErrorMsg: false
  })
}

import moment from 'moment'
import paymentMethod from '@/constants/paymentMethod'
import { capitalize } from './stringUtil.js'

import { bookingType } from '@/constants/booking'

export default {
  toVnd(value) {
    value = parseFloat(value)
    if (value) {
      return value.toLocaleString('vi-VN', {
        style: 'currency',
        currency: 'VND',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      })
    }
    return (0).toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    })
    // it-IT
    // vi-VN
  },
  format_date(value) {
    if (value) {
      return moment(String(value), 'DD/MM/yyyy HH:mm').format('DD/MM/yyyy HH:mm')
    }
  },
  dateToHour(value) {
    if (value) {
      return moment(String(value), 'DD/MM/yyyy HH:mm').format('HH:mm')
    }
  },
  getBookingTypeToColor(value) {
    switch (value) {
      case BOOKING_TYPE.HOME_AIRPORT:
        return 'light-blue'
      case BOOKING_TYPE.AIRPORT_HOME:
        return 'light-green'
      case BOOKING_TYPE.PROVINCE:
        return '#00C4C4'
      case BOOKING_TYPE.COMPOUND:
        return '#008200'
      case BOOKING_TYPE.URBAN:
        return 'light-blue'
      case BOOKING_TYPE.SELF_DRIVING:
        return 'light-blue'
      default:
        return 'common.invalid'
    }
  },
  getPaymentMethodTranslate(value) {
    switch (value) {
      case paymentMethod.CASH:
        return 'paymentMethod.cash'
      case paymentMethod.BANKING_DRIVER:
        return 'paymentMethod.banking_drive'
      case paymentMethod.BANKING_BEFORE:
        return 'paymentMethod.banking_before'
      case paymentMethod.BANKING_AFTER:
        return 'paymentMethod.banking_after'
      default:
        return 'common.invalid'
    }
  },
  getTranslateByMode(rawTranslate, mode) {
    return rawTranslate + capitalize(mode)
  },
  getPartnerMessenger(booking) {
    var result = ''

    var typeStr = ''
    switch (booking.type) {
      case bookingType.home_airport:
        typeStr = 'Xe sân bay Tiễn'
        break
      case bookingType.airport_home:
        typeStr = 'Xe sân bay Đón'
        break
      case bookingType.province:
        typeStr = 'Xe tỉnh'
        break
      case bookingType.compound:
        typeStr = 'Xe ghép'
        break

      case bookingType.rental:
        typeStr = 'Xe tự lái'
        break
    }
    result += typeStr + ' - '
    if (booking.twoWay) {
      result += '2 chiều' + ' - '
    }
    if (booking.carType) {
      result += booking.carType.name + ' - '
    }
    if (booking.timeLeave) {
      result += booking.timeLeave + ' - '
    }
    if (booking.timeReturn) {
      result += booking.timeReturn + ' - '
    }
    if (booking.flightCode) {
      result += booking.flightCode + ' - '
    }
    var startAddress = booking.bookingAddress.filter((e) => e.type == 'start')

    startAddress.forEach((address, index) => {
      if (index === startAddress.length - 1) {
        result += address.detail + ' - '
      } else {
        result += address.detail + ' + '
      }
    })
    var endAddress = booking.bookingAddress.filter((e) => e.type == 'end')
    endAddress.forEach((address, index) => {
      if (index === startAddress.length - 1) {
        result += address.detail + ' - '
      } else {
        result += address.detail + ' + '
      }
    })

    var paymentMethodStr = ''
    switch (booking.paymentMethod) {
      case paymentMethod.CASH:
        paymentMethodStr = 'Tiền mặt'
        break
      case paymentMethod.BANKING_DRIVER:
        paymentMethodStr = 'CK lái xe'
        break
      case paymentMethod.BANKING_BEFORE:
        paymentMethodStr = 'CK trước'
        break
      case paymentMethod.BANKING_AFTER:
        paymentMethodStr = 'CK sau'
        break
    }
    if (booking.user) {
      result += ` ${booking.user.userName}(${booking.user.fullName}) - `
    }
    result += paymentMethodStr + ' - '
    result += this.toVnd(booking.moneyPayment)
    return result
  }
}

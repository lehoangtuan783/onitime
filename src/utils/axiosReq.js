import store from '@/store'
import axios from 'axios'
import { ElLoading, ElMessage, ElMessageBox, ElNotification } from 'element-plus'
import { getToken, setToken } from '@/utils/auth'
import qs from 'qs'
let reqConfig
let loadingE

const service = axios.create()
// request
service.interceptors.request.use(
  (req) => {
    console.log(req)
    if (req.bfLoading) {
      store.dispatch('app/pushLoadingView')
    }
    console.log('service.interceptors.request')

    // token setting
    req.headers['Authorization'] = `Bearer ${getToken()}`
    req.headers['Accept-Language'] = `vi`
    /* download file*/
    if (req.isDownLoadFile) {
      req.responseType = 'blob'
    }
    /* upload file*/
    if (req.isUploadFile) {
      req.headers['Content-Type'] = 'multipart/form-data'
    }

    req.params = req.params
    req.data = req.data
    req.paramsSerializer = (params) => {
      return qs.stringify(params, {
        serializeDate: (date) => dayjs(date).format('DD/MM/YYYY HH:mm')
      })
    }
    console.log(req)
    //save req for res to using
    reqConfig = req
    return req
  },
  (err) => {
    store.dispatch('app/popLoadingView')
    ElMessage({
      message: err,
      type: 'error',
      duration: 2 * 1000
    })
    Promise.reject(err)
  }
)

// service.interceptors.response.use((config) => {
//   config.paramsSerializer = (params) => {
//     console.log('service.interceptors')
//     qs.stringify(params, {
//       serializeDate: (date) => dayjs(date).format('DD/MM/YYYY HH:mm'),

//       arrayFormat: 'brackets'
//     })
//   }

//   return config
// })
//response
service.interceptors.response.use(
  (res) => {
    store.dispatch('app/popLoadingView')
    if (reqConfig.afHLoading && loadingE) {
      loadingE.close()
    }
    // direct return, when download file
    if (reqConfig.isDownLoadFile) {
      return res
    }
    const { msg, status } = res

    //update token
    // if (isNeedUpdateToken) {
    //   setToken(updateToken)
    // }
    const successCode = '0,200,20000'
    // console.log(res)
    if (successCode.indexOf(status) !== -1) {
      //业务成功处理
      return res.data
    } else {
      //业务失败处理
      // if (status === 403) {
      //   if (location.href.indexOf('/login') === 1) {
      //     ElMessageBox.confirm('请重新登录', {
      //       confirmButtonText: '重新登录',
      //       cancelButtonText: '取消',
      //       type: 'warning'
      //     }).then(() => {
      //       store.dispatch('user/resetToken').then(() => {
      //         location.reload()
      //         //direct return
      //         return Promise.reject(res.data)
      //       })
      //     })
      //   }
      // }
      //是否需要提示错误信息 isAlertErrorMsg:true 提示
      if (reqConfig.isAlertErrorMsg) {
        // res.messages.join()

        ElMessage({
          message: msg,
          type: 'error',
          duration: 2 * 1000
        })
      }

      //返回错误信息
      //如果未catch 走unhandledrejection进行收集

      return Promise.reject(res)
    }
  },
  (err) => {
    /*http错误处理，处理跨域，404，401，500*/
    store.dispatch('app/popLoadingView')
    if (loadingE) loadingE.close()
    // console.log(err)
    // ElMessage({ dangerouslyUseHTMLString: true, message: err.response.data.messages.join('<br/>'), type: 'error' })
    // ElNotification({
    //   dangerouslyUseHTMLString: true,
    //   // title: 'Error',
    //   message: err.response.data.messages.join('<br/>'),
    //   type: 'error'
    // })
    //如果是跨域
    //Network Error,cross origin
    console.log(err)
    ElMessage({
      message: err.response.data.messages.join('\n'),
      type: 'error',
      duration: 2 * 1000
    })
    let errObj = {
      msg: err.toString(),
      reqUrl: reqConfig.baseURL + reqConfig.url,
      params: reqConfig.isParams ? reqConfig.params : reqConfig.data
    }
    return Promise.reject(err)
  }
)

export default function axiosReq({
  url,
  data,
  params,
  // paramsSerializer,
  method,
  bfLoading,
  afHLoading,
  isUploadFile,
  isDownLoadFile,
  baseURL,
  timeout,
  isAlertErrorMsg
}) {
  return service({
    url: url,
    method: method ?? 'get',
    data: data ?? {},
    params: params ?? {},
    // paramsSerializer: paramsSerializer,
    bfLoading: bfLoading ?? true,
    afHLoading: afHLoading ?? true,
    isUploadFile: isUploadFile ?? false,
    isDownLoadFile: isDownLoadFile ?? false,
    isAlertErrorMsg: isAlertErrorMsg ?? true,
    baseURL: baseURL ?? import.meta.env.VITE_APP_BASE_URL, // 设置基本基础url
    timeout: timeout ?? 15000 // 配置默认超时时间
  })
}

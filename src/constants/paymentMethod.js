export default Object.freeze({
  CASH: 'cash',
  BANKING_DRIVER: 'banking_drive',
  BANKING_BEFORE: 'banking_before',
  BANKING_AFTER: 'banking_after'
})

export var SYSTEM_CONFIG = {
  policy_content: 'policy_content',
  policy_terms: 'policy_terms',
  about_us: 'about_us',
  promise: 'promise',
  promotion_province: 'promotion_province',
  promotion_compound: 'promotion_compound',
  referral_value: 'referral_value',
  address_fee: 'address_fee',
  high_quality_fee: 'high_quality_fee',
  home_airport_before_5h_fee: 'home_airport_before_5h_fee',
  home_airport_before_5h_6h_fee: 'home_airport_before_5h_6h_fee',
  airport_home_22h_23h_fee: 'airport_home_22h_23h_fee',
  home_airport_after_23h_fee: 'home_airport_after_23h_fee'
}

export var SYSTEM_CONFIG_TYPE = {
  policy_content: 'policy_content',
  policy_terms: 'policy_terms',
  about_us: 'about_us'
}

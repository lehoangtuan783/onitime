export var supportType = {
  operator_onitime: 'operator_onitime',
  sms: 'sms',
  zalo: 'zalo',
  messenger: 'messenger',
  manager: 'manager',
  website: 'website',
  suggestion: 'suggestion'
}

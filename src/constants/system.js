export default {
  FORMAT_DATE: 'DD/MM/YYYY',
  FORMAT_DATE_TIME: 'DD/MM/YYYY HH:mm',
  CURRENCY_FORMAT: {
    decimal: ',',
    thousands: '.',
    prefix: '',
    suffix: ' ₫',
    precision: 0,
    masked: false /* doesn't work with directive */
  }
}

export var bookingType = {
  home_airport: 'home_airport',
  airport_home: 'airport_home',
  province: 'province',
  compound: 'compound',
  rental: 'rental'
  // URBAN: 'urban'
}

export var bookingStatus = ['received', 'arranged', 'progress', 'completed', 'cancelled', 'partner_payment']

export var bookingAddressType = ['start', 'end']

export var calendarType = ['home_airport', 'airport_home', 'province', 'compound']
export var calendarStatus = ['received', 'arranged', 'completed', 'cancelled']

export var calendarAskingType = ['home_airport', 'airport_home', 'province', 'compound']
export var calendarAskingStatus = ['received']

export var calendarRentalType = ['rental']
export var calendarRentalStatus = ['received', 'arranged', 'completed', 'cancelled']

export var bookingTypeClass = {
  home_airport: 'home-airport-row',
  airport_home: 'airport-home-row',
  province: 'province-row',
  compound: 'compound-row',
  rental: 'rental-row'
}

export var bookingTypeColor = {
  airport: 'light-green',
  home_airport: '#FF5151',
  airport_home: 'light-green',
  province: '#00C4C4',
  compound: '#008200',
  rental: 'light-blue'
}

export var carType = {
  airport: 'airport',
  province: 'province',
  compound: 'compound',
  rental: 'rental'
}

export var routeCarType = ['airport', 'province', 'compound']

export var priceListCarType = {
  airport: 'airport',
  province: 'province',
  compound: 'compound'
}

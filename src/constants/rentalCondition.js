export var rentalConditionSection = {
  license: 'license',
  deposit: 'deposit',
  when: 'when',
  distance_limited: 'distance_limited',
  payment: 'payment'
}

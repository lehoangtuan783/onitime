export var locationType = ['airport', 'location']
export var locationTypeColor = {
  airport: 'success',
  location: 'info'
}

import { createApp } from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
const app = createApp(App)
import router from './router'
import '@/styles/index.scss' // global css

//import vuex
import store from './store'
app.use(store)

//import element-plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
app.use(ElementPlus, { size: 'mini', locale: zhCn })

//global mixin(can choose by you need )
import elementMixin from '@/mixins/elementMixin'
app.mixin(elementMixin)
import commonMixin from '@/mixins/commonMixin'
app.mixin(commonMixin)
import routerMixin from '@/mixins/routerMixin'
app.mixin(routerMixin)

//import axios req
import axiosReq from '@/utils/axiosReq'
app.config.globalProperties.$axiosReq = axiosReq

//svg-icon
//import svg-icon doc in  https://github.com/anncwb/vite-plugin-svg-icons/blob/main/README.zh_CN.md
import 'virtual:svg-icons-register'
import svgIcon from '@/icons/SvgIcon.vue'
app.component('svg-icon', svgIcon)

//global mount moment-mini
import $momentMini from 'moment-mini'
app.config.globalProperties.$momentMini = $momentMini

//import global directive
import directive from '@/directive'
directive(app)

//import router  intercept
import './permission'

//error log  collection
// import errorLog from '@/hooks/useErrorLog'
// errorLog()

import i18n from './lang'
import Vue3Lottie from 'vue3-lottie'

import ManagerTemplatePage from '@/layout/components/page-components/manager-template-page.vue'

import ImageGallery from '@/views/image-gallery.vue'
import DataToolbar from '@/components/data-toolbar/index.vue'
import Pagination from '@/components/pagination/index.vue'

app.component('ManagerTemplatePage', ManagerTemplatePage)
app.component('ImageGallery', ImageGallery)
app.component('DataToolbar', DataToolbar)
app.component('Pagination', Pagination)

import { Money3Component } from 'v-money3'
app.component('Money3', Money3Component)
import CKEditor from '@ckeditor/ckeditor5-vue'

import filters from '@/utils/filters'
import system from '@/constants/system'

app.config.globalProperties.$filters = filters
app.config.globalProperties.$system = system

import VueClipboard from 'vue3-clipboard'
app.use(VueClipboard, {
  autoSetContainer: true,
  appendToBody: true
})
app.use(router)
app.use(i18n)
app.use(Vue3Lottie)
app.use(CKEditor)
app.mount('#app')

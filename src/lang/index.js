import { createI18n } from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-plus/lib/locale/lang/en' // element-ui lang
import elementZhLocale from 'element-plus/lib/locale/lang/zh-CN' // element-ui lang
import elementEsLocale from 'element-plus/lib/locale/lang/es' // element-ui lang
import elementJaLocale from 'element-plus/lib/locale/lang/ja' //
import elementViLocale from 'element-plus/lib/locale/lang/vi'
import enLocale from './en'
import zhLocale from './zh'
import esLocale from './es'
import jaLocale from './ja'
import viLocale from './vi'

const messages = {
  // en: {
  //   ...enLocale,
  //   ...elementEnLocale
  // },
  // zh: {
  //   ...zhLocale,
  //   ...elementZhLocale
  // },
  // es: {
  //   ...esLocale,
  //   ...elementEsLocale
  // },
  // ja: {
  //   ...jaLocale,
  //   ...elementJaLocale
  // },
  vi: {
    ...viLocale,
    ...elementViLocale
  }
}
export function getLanguage() {
  const chooseLanguage = Cookies.get('language')
  if (chooseLanguage) return chooseLanguage

  // if has not choose language
  const language = (navigator.language || navigator.browserLanguage).toLowerCase()
  const locales = Object.keys(messages)
  for (const locale of locales) {
    if (language.indexOf(locale) > -1) {
      return locale
    }
  }
  return 'vi'
}

const i18n = createI18n({
  legacy: false,
  locale: getLanguage(),
  fallbackLocale: 'vi',
  globalInjection: true,
  messages
})

export default i18n

export default {
  common: {
    appName: 'Onitime',
    searchHint: 'Tìm kiếm dữ liệu',
    cancel: 'Hủy',
    confirm: 'Xác nhận',
    invalid: 'NaN Data',
    filter: 'Lọc dữ liệu'
  },
  login: {
    title: 'Trang quản trị ONITIME',
    userNameHint: 'Tên tài khoản',
    passwordHint: 'Mật khẩu',
    action: 'Đăng nhập',
    forgotPasswordLead: 'Quên mật khẩu'
  },
  forgotPassword: {
    instruction: 'Bạn quên mật khẩu? Bạn có thể dễ dàng lấy lại mật khẩu ở đây',
    emailHint: 'Địa chỉ email',
    action: 'Đổi mật khẩu'
  },
  deleteImageMessage: {
    confirmMessage: 'Bạn có chắc chắn xóa ảnh này',
    confirmButtonText: 'Có',
    cancelButtonText: 'Hủy',
    successDeleteMessage: 'Ảnh đã được xóa thành công',
  },
  changePassword: {
    title: 'Đổi mật khẩu',
    password: {
      title: 'Mật khẩu cũ',
      hint: 'Mật khẩu cũ'
    },
    newPassword: {
      title: 'Mật khẩu mới',
      hint: 'Mật khẩu mới'
    },
    confirmPassword: {
      title: 'Xác nhận mật khẩu',
      hint: 'Xác nhận mật khẩu'
    },
    form: {
      passwordRequired: 'Mật khẩu cũ là bắt buộc',
      newPasswordRequired: 'Mật khẩu mới là bắt buộc',
      confirmPasswordRequired: 'Xác nhận mật khẩu là bắt buộc',
      confirmPasswordMatched: 'Mật khẩu xác nhận phải giống với mật khẩu mới'
    },
    action: 'Đổi mật khẩu'
  },
  userProfile: {
    title: 'Hồ sơ',
    name: {
      title: 'Tên tài khoản',
      hint: 'Tên tài khoản'
    },
    email: {
      title: 'Địa chỉ email',
      hint: 'Địa chỉ email'
    },
    form: {
      nameRequired: 'Tên tài khoản là bắt buộc',
      emailRequired: 'Địa chỉ email là bắt buộc'
    },
    action: 'Cập nhật hồ sơ'
  },
  page: {
    calendar: {
      title: 'Lịch',
      day: 'Theo ngày',
      asking: 'Báo giá',
      rental: 'Thuê xe',
      edit: 'Thông tin lịch'
    },
    carType: {
      title: 'Loại xe',
      manager: 'Danh sách loại xe',
      add: 'Thêm loại xe',
      edit: 'Sửa loại xe'
    },
    area: {
      title: 'Khu vực thuê xe',
      manager: 'Danh sách khu vực',
      add: 'Thêm khu vực',
      edit: 'Sửa khu vực'
    },
    rentalCondition: {
      title: 'Chính sách thuê xe',
      manager: 'Danh sách chính sách',
      add: 'Thêm chính sách',
      edit: 'Sửa chính sách'
    },
    carRental: {
      title: 'Xe thuê',
      manager: 'Danh sách xe thuê',
      add: 'Thêm xe thuê',
      edit: 'Sửa xe thuê'
    },
    partner: {
      title: 'Đối tác',
      manager: 'Danh sách đối tác',
      add: 'Thêm đối tác',
      edit: 'Sửa đối tác'
    },
    carInfo: {
      title: 'Thông tin xe',
      manager: 'Danh sách thông tin xe',
      add: 'Thêm thông tin xe',
      edit: 'Sửa thông tin xe'
    },
    location: {
      title: 'Địa điểm',
      manager: 'Danh sách địa điểm',
      add: 'Thêm địa điểm',
      edit: 'Sửa địa điểm'
    },
    route: {
      title: 'Tuyến đường',
      manager: 'Danh sách tuyến đường',
      add: 'Thêm tuyến đường',
      edit: 'Sửa tuyến đường'
    },
    priceList: {
      title: 'Bảng giá',
      manager: 'Danh sách bảng giá',
      add: 'Thêm bảng giá',
      edit: 'Sửa bảng giá'
    },
    promotionArticle: {
      title: 'Bài đăng khuyến mại',
      manager: 'Danh sách bài đăng',
      add: 'Thêm bài đăng',
      edit: 'Sửa bài đăng'
    },
    voucher: {
      title: 'Voucher',
      manager: 'Danh sách voucher',
      add: 'Thêm voucher',
      edit: 'Sửa voucher'
    },
    bankAccount: {
      title: 'Tài khoản ngân hàng',
      manager: 'Danh sách tài khoản',
      add: 'Thêm tài khoản',
      edit: 'Sửa tài khoản'
    },
    rank: {
      title: 'Hạng',
      manager: 'Danh sách hạng',
      add: 'Thêm hạng',
      edit: 'Sửa hạng'
    },
    user: {
      title: 'Người dùng',
      manager: 'Danh sách người dùng',
      add: 'Thêm người dùng',
      edit: 'Sửa người dùng'
    },
    support: {
      title: 'Liên hệ',
      manager: 'Danh sách liên hệ',
      add: 'Thêm liên hệ',
      edit: 'Sửa liên hệ'
    },
    question: {
      title: 'Câu hỏi',
      manager: 'Danh sách câu hỏi',
      add: 'Thêm câu hỏi',
      edit: 'Sửa câu hỏi'
    },
    slider: {
      title: 'Slider',
      manager: 'Danh sách slider',
      add: 'Thêm slider',
      edit: 'Sửa slider'
    },
    notificationInstruction: {
      title: 'Cài đặt thông báo',
      manager: 'Danh sách',
      add: 'Thêm cài đặt',
      edit: 'Sửa cài đặt'
    },
    config: {
      title: 'Cấu hình',
      manager: 'Danh sách cấu hình',
      add: 'Thêm cấu hình',
      edit: 'Sửa cấu hình'
    },
    notification: {
      title: 'Thông báo',
      manager: 'Danh sách thông báo',
      add: 'Gửi thông báo',
      edit: 'Sửa cấu hình'
    },
    booking: {
      title: 'Đặt xe',
      manager: 'Danh sách đặt xe',
      add: 'Thêm đặt xe',
      edit: 'Sửa đặt xe'
    },
    debug: {
      title: 'Debugger',
      imageManager: 'Kho ảnh',
      transactionManager: 'Giao dịch',
      googleManager: 'Dữ liệu Google',
      walletTransaction: {
        add: 'Tạo giao dịch'
      }
    }
  },
  manager: {
    actions: 'Hành động'
  },
  navbar: {
    searchHint: 'Tìm kiếm',
    zoomInHint: 'Phóng to',
    zoomOutHint: 'Thu nhỏ',
    globalSizeHint: 'Thay đổi cỡ chữ',
    languageHint: 'Thay đổi ngôn ngữ'
  },
  profileMenu: {
    home: 'Trang chủ',
    profile: 'Hồ sơ',
    changePassword: 'Đổi mật khẩu',
    setting: 'Cài đặt',
    logOut: 'Đăng xuất'
  },
  profileDropdown: {
    profile: 'Hồ sơ',
    logout: 'Đăng xuất'
  },
  register: {
    title: 'Đăng ký',
    register: 'Đăng ký',
    usernameHint: 'Tên đăng nhập',
    passwordHint: 'Mật khẩu',
    confirmPasswordHint: 'Xác nhận mật khẩu'
  },
  _404: {
    title: 'Không tìm thấy trang',
    main_message: 'Quản trị viên web nói rằng bạn không thể vào trang này ...',
    detail_message:
      'Vui lòng kiểm tra xem URL bạn đã nhập có chính xác không, hoặc nhấp vào nút bên dưới để quay lại trang chủ.',
    back_to_home: 'Quay lại trang chủ'
  },
  'user-form': {
    title: 'Thêm người dùng',
    'title-edit': 'Sửa người dùng',
    usernameHint: 'Tên tài khoản',
    passwordHint: 'Mật khẩu',
    fullNameHint: 'Họ và tên',
    submit: 'Thêm người dùng'
  },

  booking: {
    platform: {
      web: 'web',
      mobile_app: 'mobile'
    },
    request: {
      booking: 'Đặt xe',
      asking: 'Yêu cầu báo giá'
    },
    status: {
      received: 'Đã tiếp nhận',
      progress: 'Đang sử dụng',
      arranged: 'Đã sắp xếp xe',
      completed: 'Hoàn thành',
      cancelled: 'Đã hủy',
      partner_payment: 'Đối tác đã thanh toán'
    },
    type: {
      home_airport: 'Sân bay tiễn',
      airport_home: 'Sân bay đón',
      province: 'Xe tỉnh',
      compound: 'Xe ghép',
      rental: 'Xe tự lái',
      urban: 'Nội thành'
    },
    paymentMethod: {
      cash: 'Tiền mặt',
      banking_driver: 'CK lái xe',
      banking_before: 'CK trước',
      banking_after: 'CK sau'
    },
    vatStatus: {
      in_completed: 'Chưa xuất hóa đơn',
      completed: 'Đã xuất hóa đơn'
    },
    address: {
      type: {
        start: 'Điểm đón',
        end: 'Điểm trả'
      }
    }
  },

  partnerType: {
    partner: 'Đối tác',
    other: 'Xe khác'
  },
  asking: {
    title: 'Quản lý báo giá',
    order: 'STT',
    timeLeave: 'Giờ đón',
    partnerNotify: 'Đối tác',
    province: 'Địa bàn',
    type: 'Loại lịch',
    seats: 'Số chỗ',
    customer: 'Khách hàng',
    addressStart: 'Điểm đi',
    addressEnd: 'Điểm đến',
    moneyPayment: 'Số tiền',
    moneyPromotion: 'Khuyến mại',
    moneyTransfer: 'Đã thanh toán',
    outputInfo: 'Nội dung cho đối tác'
  },
  bookingForm: {
    titleAdd: 'Tạo chuyến',
    titleEdit: 'Chỉnh sửa chuyến',
    type: 'Kiểu chuyến',

    infoSection: 'Thông tin chuyến',
    carType: 'Loại xe',
    timeLeave: 'Giờ đi',
    timeReturn: 'Giờ về',
    twoWay: 'Đi 2 chiều',
    paymentMethod: 'Phương thức thanh toán',
    seats: 'Số ghế',
    highQuality: 'Yêu cầu chất lượng cao',
    start: 'Điểm đón',
    end: 'Điểm đến',

    moneySection: 'Thông tin giá',
    moneyRouteEdit: 'Sửa số tiền chuyến',
    moneyDepositedEdit: 'Sửa đặt cọc',
    moneyTransferredEdit: 'Sửa đã chuyển',
    moneyRoute: 'Số tiền chuyến',
    moneyAddress: 'Phí địa điểm',
    moneyHighQuality: 'Phí chất lượng cao',
    moneyPromotion: 'Số tiền khuyến mại',
    moneyDeposited: 'Đã đặt cọc',
    moneyTransferred: 'Đã chuyển',
    moneyPayment: 'Phải thanh toán',

    vatSection: 'Thông tin xuất hóa đơn',
    vatInvoice: 'Yêu cầu xuất hóa đơn',
    vatName: 'Tên',
    vatAddress: 'Địa chỉ',
    vatTaxNumber: 'Mã số thuê',
    vatEmail: 'Địa chỉ email',
    carInfoSection: 'Thông tin sắp xếp xe',
    carInfoSectionEdit: 'Sửa thông tin xe',
    carInfoName: 'Tên xe',
    carInfoDriverName: 'Tên tài xế',
    carInfoDetail: 'Thông tin chi tiết',
    carInfoPhoneNumber: 'Số điện thoại',

    moneyRouteRequired: 'Số tiền chuyến cần cập nhật là bắt buộc!',
    moneyDepositedRequired: 'Số tiền đặt cọc cần cập nhật là bắt buộc!',
    moneyDepositedRange: 'Số tiền đặt cọc không hợp lệ!',
    moneyTransferredRequired: 'Số tiền đã chuyển cần cập nhật là bắt buộc!',
    moneyTransferredRange: 'Số tiền đã chuyển không hợp lệ!',

    actionAdd: 'Thêm',
    actionEdit: 'Sửa'
  },

  calendar: {
    rental: {
      rentalDateStart: 'Ngày nhận xe',
      rentalDateEnd: 'Ngày trả xe',
      userName: 'Khách hàng',
      fullName: 'Họ tên',
      rentalLocation: 'Địa điểm nhận xe',
      moneyPayment: 'Số tiền',
      partner: 'Đối tác',
      carRental: 'Xe đặt',
      carInfo: 'Xe thuê',
      status: 'Trạng thái',
      note: 'Ghi chú'
    },
    manager: {
      title: 'Quản lý chuyến',
      order: 'STT',
      timeLeave: 'Giờ đón',
      status: 'Trạng thái',
      area: 'Địa bàn',
      province: 'Địa bàn',
      type: 'Loại lịch',
      carType: 'Loại xe',
      customer: 'Khách hàng',
      addressStart: 'Điểm đi',
      addressEnd: 'Điểm đến',
      moneyPayment: 'Số tiền',
      moneyPromotion: 'Khuyến mại',
      moneyTransfer: 'Đã thanh toán',
      paymentMethod: 'Phương thức TT',
      carInfo: 'Xe đón',
      outputInfo: 'Nội dung cho đối tác',
      filters: {
        title: 'Lọc dữ liệu chuyến',
        timeLeave: 'Ngày đi',
        type: 'Kiểu đặt chuyến',
        status: 'Trạng thái chuyến',
        startDate: 'Từ ngày',
        endDate: 'Đến ngày',
        status: 'Trạng thái',
        partner: 'Đối tác'
      }
    },
    bookingCarInfo: {
      title: 'Xếp xe',
      form: {
        partner: 'Đối tác',
        driverName: 'Tên tài xế',
        driverNameRequired: 'Tên tài xế là bắt buộc!',
        name: 'Tên xe',
        nameRequired: 'Tên xe là bắt buộc!',
        number: 'Biểm kiểm soát',
        numberRequired: 'Biểm kiểm soát là bắt buộc!',
        phoneNumber: 'Số điện thoại',
        phoneNumberRequired: 'Số điện thoại là bắt buộc!',
        partnerName: 'Tên đối tác',
        partnerCar: 'Danh sách xe'
      }
    },
    status: {
      title: 'Trạng thái',
      status: 'Trạng thái hiện tại',
      form: {
        status: 'Trạng thái',
        statusRequired: 'Trạng thái là bắt buộc!'
      }
    },
    moneyRoute: {
      title: 'Tiền chuyến',
      chargeRoute: 'Tiền chuyến hiện tại',
      form: {
        chargeRoute: 'Tiền chuyến',
        chargeRouteRequired: 'Tiền chuyến là bắt buộc!'
      }
    },
    moneyDeposited: {
      title: 'Tiền cọc',
      moneyDeposited: 'Tiền cọc hiện tại',
      form: {
        moneyDeposited: 'Tiền cọc',
        moneyDepositedRequired: 'Tiền cọc là bắt buộc!'
      }
    },
    moneyTransferred: {
      title: 'Tiền đã chuyển',
      moneyTransferred: 'Tiền đã chuyển khoản hiện tại',
      form: {
        moneyTransferred: 'Tiền đã chuyển khoản ',
        moneyTransferredRequired: 'Tiền đã chuyển khoản là bắt buộc!'
      }
    },
    bookingAddress: {
      title: 'Điểm đón/trả',
      bookingAddress: 'Điểm đón/trả hiện tại',
      add: {
        title: 'Thêm điểm đón/trả',
        form: {
          type: 'Kiểu địa điểm',
          typeRequired: 'Kiểu địa điểm là bắt buộc!',
          order: 'Thứ tự',
          orderRequired: 'Thứ tự là bắt buộc!',
          detail: 'Tên địa điểm',
          detailRequired: 'Tên địa điểm là bắt buộc!'
        }
      },
      form: {
        bookingAddress: 'Điểm đón/trả ',
        bookingAddressRequired: 'Điểm đón/trả là bắt buộc!'
      }
    },
    timeLeave: {
      title: 'Thời gian đón',
      timeLeave: 'Thời gian đón hiện tại',
      form: {
        timeLeave: 'Thời gian đón',
        timeLeaveRequired: 'Thời gian đón là bắt buộc!'
      }
    },
    carType: {
      title: 'Loại xe',
      carType: 'Loại xe hiện tại',
      form: {
        carType: 'Loại xe',
        carTypeRequired: 'Loại xe là bắt buộc!'
      }
    },
    form: {
      createdAt: 'Thời gian đặt',
      type: 'Kiểu đặt chuyến',
      request: 'Yêu cầu',
      status: 'Trạng thái',
      platform: 'Nền tảng',
      user: {
        title: 'Người dùng',
        userName: 'Tài khoản',
        fullName: 'Họ tên'
      },
      carType: {
        title: 'Loại xe đặt',
        name: 'Tên',
        description: 'Chi tiết',
        seats: 'Số ghế'
      },
      bookingAddress: {
        title: 'Địa điểm',
        start: 'Đón khách tại',
        end: 'Trả khách tại'
      },
      timeLeave: 'Giờ đi',
      timeReturn: 'Giờ đón',
      flightCode: 'Mã chuyến bay',
      twoWay: '2 chiều',
      paymentMethod: 'Phương thức thanh toán',
      note: 'Ghi chú',
      highQuality: 'Chất lượng cao',
      carRental: {
        title: 'Loại xe thuê',
        name: 'Tên',
        carType: 'Loại xe',
        charge: 'Phí thuê',
        surcharge: 'Phụ phí'
      },
      rentalDateStart: 'Ngày thuê xe',
      rentalDateEnd: 'Ngày trả xe',
      rentalLocation: 'Địa điểm nhận xe',
      bookingCarInfo: {
        title: 'Xe đã sắp xếp',
        name: 'Tên xe',
        driverName: 'Tài xế',
        number: 'Biến kiểm soát',
        phoneNumber: 'Số điện thoại'
      },
      rating: {
        title: 'Đánh giá',
        point: 'Điểm đánh giá',
        note: 'Ghí chú',
        feedbacks: 'Phản ánh'
      },
      vatStatus: 'Tình trạng xhđ',
      vatInvoice: 'Yêu cầu xhđ',
      vat: {
        title: 'Thông tin xhđ'
      },
      vatName: 'Tên',
      vatAddress: 'Địa chỉ',
      vatTaxNumber: 'Mã số thuế',
      vatEmail: 'Email',
      charge: {
        title: 'Chi tiết tính phí',
        route: 'Tuyến đường',
        rental: 'Thuê xe',
        surchargeRental: 'Phụ phí',
        time: 'Thời gian',
        address: 'Điểm đón',
        highQuality: 'Chất lượng cao',
        promotionLimited: 'Khuyến mãi tối đa',
        promotionWallet: 'Khuyến mãi ví',
        promotionRankAddress: 'Miễn phí điểm đón/trả'
      },
      money: {
        title: 'Chi tiết thanh toán',
        total: 'Tổng đơn',
        payment: 'Phải thanh toán',
        promotion: 'Giảm giá',
        deposited: 'Đã đặt cọc',
        transferred: 'Đã chuyển khoản',
        owed: 'Còn nợ'
      },

      true: 'Có',
      false: 'Không'
    }
  },
  carType: {
    type: {
      airport: 'Sân bay',
      province: 'Xe tỉnh',
      compound: 'Xe ghép',
      rental: 'Xe tự lái',
      urban: 'Nội thành'
    },
    manager: {
      order: 'STT',
      name: 'Tên',
      description: 'Mô tả',
      seats: 'Số ghế',
      type: 'Kiểu',
      deleteTitle: 'Xóa loại xe',
      deleteMessage: 'Bạn có chắc chắn muốn xóa loại xe không?'
    },
    form: {
      image: 'Ảnh loại xe',
      imageRequired: 'Ảnh loại xe là bắt buộc!',
      name: 'Tên loại xe',
      nameRequired: 'Tên loại xe là bắt buộc!',
      description: 'Mô tả',
      seats: 'Số ghế',
      seatsRequired: 'Số ghế là bắt buộc!',
      type: 'Kiểu xe',
      typeRequired: 'Kiểu xe là bắt buộc!'
    }
  },
  area: {
    manager: {
      title: 'Quản lý khu vực thuê xe',
      order: 'STT',
      name: 'Tên',
      rentalSurcharge: 'Phụ phí cuối tuần',
      deleteTitle: 'Xóa khu vực',
      deleteMessage: 'Bạn có chắc chắn muốn xóa khu vực không?'
    },
    form: {
      name: 'Tên khu vực',
      nameRequired: 'Tên khu vực là bắt buộc!',
      rentalSurcharge: 'Phụ phí khu vực',
      rentalSurchargeRequired: 'Phụ phí khu vực là bắt buộc!'
    }
  },
  rentalCondition: {
    section: {
      license: 'GIẤY TỜ CẦN CÓ',
      deposit: 'ĐẶT CỌC',
      when: 'THỜI ĐIỂM NHẬN, TRẢ XE',
      distance_limited: 'KM GIỚI HẠN',
      payment: 'THANH TOÁN'
    },
    manager: {
      title: 'Chính sách thuê xe',
      order: 'STT',
      area: 'Tỉnh áp dụng',
      section: 'Mục chính sách',
      value: 'Nội dung',
      delete: {
        title: 'Xóa chính sách',
        message: 'Bạn có chắc chắn muốn xóa chính sách không?'
      }
    },
    form: {
      areaId: 'Tỉnh áp dụng',
      areaIdRequired: 'Tỉnh áp dụng là bắt buộc!',
      section: 'Mục chính sách',
      sectionRequired: 'Mục chính sách là bắt buộc!',
      value: 'Nội dung',
      valueRequired: 'Nội dung là bắt buộc!'
    },
    filters: {
      title: 'Bộ lọc chính sách',
      areaId: 'Tỉnh áp dụng'
    }
  },
  carRental: {
    manager: {
      order: 'STT',
      name: 'Tên',
      charge: 'Phí thuê xe',
      surcharge: 'Phụ phí',
      carType: 'Loại xe',
      area: 'Tỉnh thành phố',
      deleteTitle: 'Xóa xe',
      deleteMessage: 'Bạn có chắc chắn muốn xóa xe không?'
    },
    form: {
      name: 'Tên xe',
      nameRequired: 'Tên xe là bắt buộc!',
      charge: 'Phí thuê xe',
      chargeRequired: 'Phí thuê xe là bắt buộc!',
      surcharge: 'Phụ phí cuối tuần',
      surchargeRequired: 'Phụ phí cuối tuần là bắt buộc!',
      carTypeId: 'Loại xe',
      carTypeIdRequired: 'Loại xe là bắt buộc!',
      areaId: 'Khu vực hoạt động',
      areaIdRequired: 'Khu vực hoạt động là bắt buộc!'
    }
  },
  partner: {
    manager: {
      order: 'STT',
      name: 'Tên đối tác',
      area: 'Địa bàn',
      phoneNumber: 'Số điện thoại',
      deleteTitle: 'Xóa đối tác',
      deleteMessage: 'Bạn có chắc chắn muốn xóa đối tác này không?'
    },
    form: {
      name: 'Tên đối tác',
      nameRequired: 'Tên đối tác là bắt buộc!',
      areaId: 'Địa bàn hoạt động',
      areaIdRequired: 'Địa bàn hoạt động là bắt buộc!',
      phoneNumber: 'Số điện thoại',
      phoneNumberRequired: 'Số điện thoại là bắt buộc!',
      actionAdd: 'Thêm',
      actionEdit: 'Sửa'
    }
  },
  carInfo: {
    type: {
      partner: 'Xe đối tác',
      rental: 'Xe tự lái'
    },
    manager: {
      order: 'STT',
      partner: 'Đối tác',
      type: 'Kiểu',
      info: 'Thông tin xe',
      name: 'Tên xe',
      driverName: 'Tên tài xế',
      number: 'Biển kiểm soát',
      phoneNumber: 'Số điện thoại',
      partner: 'Đối tác',
      rentalFee: 'Giá thuê 1h',
      deleteTitle: 'Xóa thông tin xe',
      deleteMessage: 'Bạn có chắc chắn muốn xóa thông tin xe này không?'
    },
    form: {
      type: 'Kiểu xe',
      typeRequired: 'Kiểu xe là bắt buộc!',
      name: 'Tên xe',
      nameRequired: 'Tên xe là bắt buộc!',
      driverName: 'Tên tài xế',
      driverNameRequired: 'Tên tài xế là bắt buộc!',
      number: 'Biển kiểm soát',
      numberRequired: 'Biển kiểm soát là bắt buộc!',
      phoneNumber: 'Số điện thoại',
      phoneNumberRequired: 'Số điện thoại là bắt buộc!',
      partnerId: 'Đối tác',
      partnerIdRequired: 'Đối tác là bắt buộc!',
      carTypeId: 'Loại xe',
      carTypeIdRequired: 'Loại xe là bắt buộc!',
      province: 'Địa bàn',
      provinceRequired: 'Địa bàn là bắt buộc!',
      rentalFee: 'Chi phí thuê',
      rentalFeeRequired: 'Chi phí thuê là bắt buộc!'
    }
  },
  location: {
    type: {
      airport: 'Sân bay',
      location: 'Địa điểm'
    },
    manager: {
      airport: 'Sân bay',
      location: 'Địa điểm',
      province: 'Thành phố',
      district: 'Quận/Huyện',
      ward: 'Phường/Xã',
      route: 'Đường/Tổ dân phố',
      type: 'Kiểu',
      deleteTitle: 'Xóa địa điểm',
      deleteMessage: 'Bạn có chắc chắn muốn xóa địa điểm không?'
    },
    form: {
      type: 'Kiểu địa điểm',
      typeRequired: 'Kiểu địa điểm là bắt buộc!',
      airport: 'Tên sân bay',
      airportRequired: 'Tên sân bay là bắt buộc!',
      promotionValue: 'Khuyến mại dành cho sân bay',
      promotionValueRequired: 'Khuyến mại dành cho sân bay là bắt buộc!',
      province: 'Cấp tỉnh',
      provinceRequired: 'Cấp tỉnh là bắt buộc!',
      district: 'Cấp huyện',
      districtRequired: 'Cấp huyện là bắt buộc!',
      ward: 'Cấp xã',
      wardRequired: 'Cấp xã là bắt buộc!',
      route: 'Cấp đường/tổ dân phố',
      routeRequired: 'Cấp xã là bắt buộc!'
    }
  },
  route: {
    type: {
      airport: 'Tuyến sân bay',
      location: 'Tuyến đường'
    },
    manager: {
      order: 'STT',
      type: 'Kiểu',
      start: 'Điểm đi',
      end: 'Điểm đến',
      carType: 'Loại xe',
      estimatedFee: 'Chi phí chuyến xe',
      deleteTitle: 'Xóa tuyến đường',
      deleteMessage: 'Bạn có chắc chắn muốn xóa tuyến đường không?'
    },
    form: {
      startId: 'Điểm đi',
      startIdRequired: 'Điểm đi là bắt buộc!',
      startIdHint: 'Chọn điểm bắt đầu',
      endId: 'Điểm đến',
      endIdRequired: 'Điểm đến là bắt buộc!',
      endIdHint: 'Chọn điểm kết thúc',
      distance: 'Khoảng cách quãng đường',
      distanceRequired: 'Khoảng cách quãng đường là bắt buộc!',
      estimatedFee: 'Chi phí chuyến xe',
      estimatedFeeRequired: 'Chi phí chuyến xe là bắt buộc!',
      locationSearch: 'Tìm kiếm địa điểm',
      carTypeSearch: 'Tìm kiếm loại xe',
      bookingType: 'Loại đặt xe',
      bookingTypeRequired: 'Loại đặt xe là bắt buộc!',
      reverse: 'Tạo chiều ngược lại'
    }
  },
  priceList: {
    manager: {
      title: 'Bảng giá',
      order: 'STT',
      province: 'Tỉnh',
      type: 'Kiểu đặt xe',
      routeName: 'Tuyến đường',
      seat: 'Loại xe',
      carName: 'Loại xe',
      priceRange: 'Khoảng giá',
      delete: {
        title: 'Xóa thông bảng giá',
        message: 'Bạn có chắc chắn muốn xóa bảng giá này không?'
      }
    },
    form: {
      province: 'Tỉnh/Thành phố',
      provinceRequired: 'Tỉnh/Thành phố là bắt buộc!',
      type: 'Kiểu đặt xe',
      typeRequired: 'Kiểu đặt xe là bắt buộc!',
      seat: 'Số ghế',
      seatRequired: 'Số ghế là bắt buộc!',
      routeName: 'Tên tuyến đường',
      routeNameRequired: 'Tên tuyến đường là bắt buộc!',
      priceRange: 'Khoảng giá',
      priceRangeRequired: 'Khoảng giá là bắt buộc!',
      enable: {
        title: 'Trạng thái',
        one: 'Bật',
        off: 'tắt'
      }
    }
  },
  promotionArticle: {
    manager: {
      order: 'STT',
      title: 'Tiêu đề khuyến mại',
      content: 'Nội dung khuyến mại',
      image: 'Ảnh Banner',
      createdAt: 'Ngày đăng',
      close: {
        title: 'Xóa đóng bài đăng',
        message: 'Bạn có muốn đóng bài đăng!'
      },
      delete: {
        title: 'Xóa bài đăng',
        message: 'Bạn có muốn xóa bài đăng!'
      }
    },
    form: {
      image: 'Ảnh banner',
      imageRequired: 'Ảnh banner là bắt buộc!',
      title: 'Tiêu đề',
      titleRequired: 'Tiêu đề là bắt buộc!',
      content: 'Nội dung',
      contentRequired: 'Nội dung là bắt buộc!'
    }
  },
  voucher: {
    manager: {
      order: 'STT',
      id: 'Mã voucher',
      value: 'Giá trị',
      remaining: 'Số lượng',
      startDate: 'Ngày dùng',
      endDate: 'Hạn dùng',
      delete: {
        title: 'Xóa voucher',
        message: 'Bạn có muốn xóa voucher!'
      }
    },
    form: {
      id: 'Mã voucher',
      idRequired: 'Giá trị là bắt buộc!',
      value: 'Giá trị',
      valueRequired: 'Giá trị là bắt buộc!',
      description: 'Chi tiết khuyến mại',
      remaining: 'Số lượng lần dùng',
      remainingRequired: 'Số lượng lần dùng là bắt buộc!',
      startDate: 'Ngày sử dụng',
      startDateRequired: 'Ngày sử dụng là bắt buộc!',
      endDate: 'Ngày hết hạn',
      endDateRequired: 'Ngày hết hạn là bắt buộc!'
    }
  },

  bankAccount: {
    manager: {
      order: 'STT',
      number: 'Số thẻ ',
      bankName: 'Ngân hàng',
      holder: 'Chủ thẻ',
      content: 'Nội dung CK',
      delete: {
        title: 'Xóa tài khoản ngân hàng',
        message: 'Bạn có muốn xóa tài khoản ngân hàng!'
      }
    },
    form: {
      number: 'Số thẻ ngân hàng',
      numberRequired: 'Số thẻ ngân hàng là bắt buộc!',
      bankName: 'Tên ngân hàng',
      bankNameRequired: 'Tên ngân hàng là bắt buộc!',
      holder: 'Chủ thẻ',
      holderRequired: 'Chủ thẻ là bắt buộc!',
      content: 'Nội dung chuyển khoản',
      contentRequired: 'Nội dung chuyển khoản là bắt buộc!'
    }
  },
  rank: {
    type: {
      0: 'Hạng thường',
      1: 'Hạng vàng',
      2: 'Hạng kim cương'
    },
    manager: {
      order: 'STT',
      level: 'Tên',
      point: 'Điểm yêu cầu',
      promotion: 'Hoàn tiền(%)',
      freeAddress: 'Miễn phí điểm đón/trả',
      delete: {
        title: 'Xóa liên hệ',
        message: 'Bạn có muốn xóa liên hệ!'
      }
    },
    form: {
      type: 'Hạng',
      point: 'Số chuyến yêu cầu',
      pointRequired: 'Số chuyến yêu cầu là bắt buộc!',
      promotion: 'Số tiền hoàn lại(%)',
      promotionRequired: 'Số tiền hoàn lại là bắt buộc!',
      freeAddress: 'Miễn phí điểm đón/trả',
      freeAddressRequired: 'Miễn phí điểm đón/trả là bắt buộc!',
      submit: 'Sửa xếp hạng'
    }
  },
  user: {
    role: {
      admin: 'Quản trị viên',
      staff: 'Nhân viên',
      customer: 'Khách hàng'
    },
    status: {
      actived: 'Hoạt động',
      no_active: 'Không hoạt động',
      blocked: 'Đã bị khóa'
    },
    manager: {
      order: 'STT',
      userName: 'Tài khoản',
      status: 'Trạng thái',
      fullName: 'Họ Tên',
      rank: 'Hạng',
      wallet: 'Ví',
      createdAt: 'Ngày tạo',
      value: 'Giá trị'
    },
    add: {
      userName: 'Tên người dùng',
      userNameRequired: 'Tên người dùng là bắt buộc!',
      password: 'Mật khẩu',
      passwordRequired: 'Mật khẩu là bắt buộc!',
      role: 'Vai trò',
      roleRequired: 'Vai trò là bắt buộc!'
    },
    edit: {
      status: {
        title: 'Trạng thái',
        form: {
          status: 'Trạng thái tài khoản',
          statusRequired: 'Trạng thái tài khoản là bắt buộc!'
        }
      },
      createdAt: 'Ngày tạo',
      userName: 'Tên tài khoản',
      fullName: 'Họ Tên',
      phoneNumber: 'Số điện thoại',
      otpCode: 'Mã Otp',
      wallet: {
        title: 'Ví khuyến mại',
        balance: 'Số dư tài khoản',
        balance: 'Số dư tài khoản'
      },
      rankWallet: {
        title: 'Xếp hạng của người dùng',
        rank: {
          title: 'Hạng hiện tại ',
          form: {
            title: 'Tạo hạng thủ công',
            rankId: 'Hạng'
          }
        },
        rank_period: {
          title: 'Kì xét hạng',
          notFound: 'Chưa có kì xét hạng',
          form: {
            title: 'Tạo mới kỳ xét hạng',
            timeRequired: 'Thời gian là bắt buộc',
            timeStart: 'Thời gian bắt đầu',
            timeEnd: 'Thời gian kết thúc',
            create: 'Tạo kỳ xét hạng'
          }
        }
      }
    },
    userManager: {
      order: 'STT',
      title: 'Quản lý người dùng',
      userName: 'Tài khoản',
      status: 'Trạng thái',
      fullName: 'Họ Tên',
      createdAt: 'Ngày tạo',
      value: 'Giá trị'
    },
    userDetail: {},
    bookingSection: 'Thông tin đặt chuyến'
  },
  support: {
    platform: {
      phone_number: 'Số điện thoại',
      website: 'Trang Web',
      social_network: 'Mạng xã hội',
      address: 'Địa chỉ',
      email: 'Địa chỉ Email',
      sms: 'Sms',
      sms_app: 'Ứng dụng tin nhắn'
    },
    type: {
      operator_onitime: 'Tổng đài viên',
      manager: 'Phản ánh với Quản lý công ty',
      suggestion: 'Góp ý cải thiện dịch vụ',
      website: 'Website',
      email: 'Email',
      sms: 'SMS',
      address: 'Địa chỉ',
      phone: 'Số điện thoại',
      zalo: 'Zalo',
      whatapp: 'whatapp',
      messenger: 'Messenger',
      telegram: 'telegram',
      kakaotalk: 'kakaotalk',
      line: 'line',
      wechat: 'wechat',
      imessage: 'imessage',
      facebook: 'facebook',
      youtube: 'youtube',
      twitter: 'twitter',
      instagram: 'instagram',
      pinterest: 'pinterest'
    },

    manager: {
      order: 'STT',
      platform: 'Nền tảng',
      type: 'Kiểu',
      type_title: 'Liên hệ',
      contact: 'Chi tiết',
      delete: {
        title: 'Xóa liên hệ',
        message: 'Bạn có muốn xóa liên hệ!'
      }
    },
    form: {
      platform: 'Nền tảng',
      type: 'Kiểu liên hệ',
      typeRequired: 'Kiểu liên hệ là bắt buộc!',
      contact: 'Liên hệ',
      contactRequired: 'Liên hệ là bắt buộc!'
    }
  },
  question: {
    manager: {
      order: 'STT',
      title: {
        title: 'Câu hỏi',
        vi: 'Tiếng Việt',
        en: 'Tiếng Anh'
      },
      answer: {
        title: 'Câu trả lời',
        vi: 'Tiếng Việt',
        en: 'Tiếng Anh'
      },
      delete: {
        title: 'Xóa liên hệ',
        message: 'Bạn có muốn xóa câu hỏi!'
      }
    },
    form: {
      title: {
        vi: 'Câu hỏi tiếng Việt',
        en: 'Câu hỏi tiếng Anh'
      },
      answer: {
        vi: 'Câu trả lời tiếng Việt',
        en: 'Câu trả lời tiếng Anh'
      },
      titleRequired: {
        vi: 'Câu hỏi tiếng Việt là bắt buộc!',
        en: 'Câu hỏi tiếng Anh là bắt buộc!'
      },
      answerRequired: {
        vi: 'Câu trả lời tiếng Việt là bắt buộc!',
        en: 'Câu trả lời tiếng Anh là bắt buộc!'
      }
    }
  },
  notificationInstruction: {
    manager: {
      order: 'STT',
      phoneBrand: 'Nhãn hiệu',
      images: 'Số lượng ảnh',
      delete: {
        title: 'Xóa hướng dẫn cài đặt',
        message: 'Bạn có muốn xóa hướng dẫn cài !'
      }
    },
    form: {
      phoneBrand: 'Nhãn hiệu',
      phoneBrandRequired: 'Nhãn hiệu là bắt buộc!',
      images: 'Danh sách ảnh',
      imagesRequired: 'Danh sách ảnh là bắt buộc!'
    }
  },
  slider: {
    id: {
      app_home: 'Trang chủ ứng dụng',
      app_promise: 'Cam kết',
      web_home_intro: 'Web: trang chủ phần giới thiệu',
      web_about_us_gallery: 'Web: Về chúng tối web phần thư viện ảnh'
    },
    manager: {
      order: 'STT',
      id: 'Vị trí ảnh',
      length: 'Số lượng ảnh',
      delete: {
        title: 'Xóa slider',
        message: 'Bạn có muốn xóa slider!'
      }
    },
    form: {
      id: 'Vị trí ảnh',
      idRequired: 'Vị trí ảnh là bắt buộc!',
      images: 'Danh sách ảnh',
      imagesRequired: 'Danh sách ảnh là bắt buộc!'
    }
  },

  provinceMain: {
    haNoi: 'Hà Nội',
    nhaTrang: 'Nha Trang',
    hoChiMinh: 'Hồ Chí Minh',
    phuQuoc: 'Phú Quốc',
    daNang: 'Đà Nẵng'
  },
  config: {
    section: {
      fee: 'Tiền phí',
      bonus: 'Tiền thưởng',
      info: 'Thông tin ứng dụng'
    },
    key: {
      store_android_link: 'Link Google Play',
      store_ios_link: 'Link Apple Store',
      policy_content: 'Nội dung chính sách',
      about_us: 'Về chúng tôi',
      policy_and_terms: 'Điều khoản sử dụng',
      promise: 'Cam kết',
      promotion_province: 'Khuyến mại tối đa xe tỉnh(%)',
      promotion_compound: 'Khuyến mại tối đa xe ghép(%)',
      referral_value: 'Tiền thưởng giới thiệu',
      fee_address: 'Phí điểm đón(x ₫/1 điểm)',
      fee_high_quality: 'Phí chất lượng cao(₫)',
      fee_home_airport_5h_6h: 'Phí thời gian trước 5h(₫)',
      fee_home_airport_before_5h: 'Phí thời gian trước 5h -> 6h(₫)',
      fee_airport_home_22h_23h: 'Phí thời gian trước 22h -> 23h(₫)',
      fee_airport_home_after_23h: 'Phí thời gian sau 23h(₫)',
      bonus_referral: 'Tiền thưởng giới thiệu',
      bonus_download: 'Tiền thưởng cài đặt ứng dụng'
    },
    manager: {
      order: 'STT',
      key: 'Biến cài đặt',
      section: 'Mục',
      value: 'Giá trị'
    },
    form: {
      section: 'Mục cài đặt',
      sectionRequired: 'Mục cài đặt là bắt buộc!',
      key: 'Biến cài đặt',
      keyRequired: 'Biến cài đặt là bắt buộc!',
      value: 'Giá trị',
      valueRequired: 'Giá trị là bắt buộc!'
    }
  },
  walletTransaction: {
    add: {
      user: 'Người dùng',
      userRequired: 'Người dùng là bắt buộc!',
      value: 'Giá trị',
      valueRequired: 'Giá trị là bắt buộc!'
    }
  },
  configManager: {
    order: 'STT',
    title: 'Cài đặt hệ thống',
    section: 'Mục',
    value: 'Giá trị'
  },
  configForm: {
    titleAdd: 'Tạo cấu hình',
    titleEdit: 'Chỉnh sửa cấu hình',
    section: 'Mục',
    sectionRequired: 'Mục cài đặt là bắt buộc!',
    value: 'Giá trị',
    valueRequired: 'Giá trị cài đặt là bắt buộc!',
    actionAdd: 'Thêm',
    actionEdit: 'Sửa'
  },

  notification: {
    to: {
      all: 'Tất cả người dùng',
      list: 'Danh sách người dùng',
      rank: 'Theo xếp hạng'
    },
    form: {
      title: {
        vi: 'Tiêu đề tiếng Việt',
        en: 'Tiêu đề tiếng Anh'
      },
      content: {
        vi: 'Nội dung tiếng Việt',
        en: 'Nội dung tiếng '
      },
      titleRequired: {
        vi: 'Tiêu đề tiếng Việt là bắt buộc!',
        en: 'Tiêu đề tiếng Anh là bắt buộc!'
      },
      contentRequired: {
        vi: 'Nội dung tiếng Việt là bắt buộc!',
        en: 'Nội dung tiếng Anh là bắt buộc!'
      },
      to: 'Đối tượng gửi',
      toRequired: 'Đối tượng gửi là bắt buộc',
      userIds: 'Danh sách người dùng',
      userIdsRequired: 'Danh sách người dùng là bắt buộc',
      rankId: 'Hạng người dùng',
      rankIdRequired: 'Hạng người dùng là bắt buộc',
      withVoucher: 'Gửi kèm voucher',
      voucherId: 'Chọn mã voucher'
    }
  }
}

import Mock from "mockjs";

const data = Mock.mock({
  "items|30": [
    {
      id: "@id",
      fullName: "@cname",
      "roles|1": ["Admin", "Customer", "Driver"],
      createdAt: "@datetime",
    },
  ],
});

export default [
  {
    url: "/mock/users",
    method: "get",
    response: () => {
      const items = data.items;
      return {
        code: 20000,
        data: items,
      };
    },
  },
];
